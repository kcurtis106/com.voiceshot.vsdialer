package com.voiceshot.vsdialer;

import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_FOUR;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_ONE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_THREE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_TWO;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_TWO_VM;
import static com.voiceshot.vsdialer.CommonUtilities.SERVER_URL;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.ServerUtilities.getDNIS;
import static com.voiceshot.vsdialer.ServerUtilities.getNickName;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPIN;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPhone;
import static com.voiceshot.vsdialer.ServerUtilities.notifyVMbox;


import com.voiceshot.vsdialer.PullToRefreshListView;
import com.voiceshot.vsdialer.PullToRefreshListView.OnRefreshListener;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


public class vm_new_box_activity   extends Activity {
	private PullToRefreshListView list;	
	private VMDBProvider mDbHelper;
	private Cursor vm_box_cursor;
	ProgressDialog mProgressDialog;
	private ArrayList<String> boxArray = new ArrayList<String>();
	private ArrayList<String> pinArray = new ArrayList<String>();	
	static final int DELETE_VM_BOX_MENU = 1;
	static final int DIAL_VM_BOX_MENU = 2;
	
	
	public void onCreate(Bundle savedInstanceState) { 
        super.onCreate(savedInstanceState);
	    
	    
	}
	

	
    public void savedClick(View v) {	    	
    	int position = list.getPositionForView(v)-1;	
    	vm_box_cursor.moveToPosition(position); 
    	Intent VMBoxIntent = new Intent(getParent(), boxActivity.class);
    	VMBoxIntent.putExtra("box", vm_box_cursor.getString(0));
    	VMBoxIntent.putExtra("isNew", 0);
        TabGroupActivity parentActivity = (TabGroupActivity)getParent();
        parentActivity.startChildActivity("NewActivity", VMBoxIntent);    	    	
     }	 

    public void newClick(View v) {	    	
    	int position = list.getPositionForView(v)-1;	
    	vm_box_cursor.moveToPosition(position);  
    	Intent VMBoxIntent = new Intent(getParent(), boxActivity.class);
    	VMBoxIntent.putExtra("box", vm_box_cursor.getString(0));
    	VMBoxIntent.putExtra("isNew", 1);    	
        TabGroupActivity parentActivity = (TabGroupActivity)getParent();
        parentActivity.startChildActivity("NewActivity", VMBoxIntent);  
     }
    
    public void MBClick(View v) {	    	
    	int position = list.getPositionForView(v)-1;
    	vm_box_cursor.moveToPosition(position); 
        String boxnum = vm_box_cursor.getString(0);
        int boxPos = 0;
    	for (int i = 0; i < boxArray.size(); i++) {
    		String curBox = boxArray.get(i);
    		if (curBox.contentEquals(boxnum)){
    			boxPos = i;
    			break;
    		}
    	}  
    	if (boxPos >= 0) {
  		  boxArray.remove(boxPos);
  		  pinArray.remove(boxPos);
  		  setVMBoxes(true);   
  		  mDbHelper.deleteBoxEntry(boxnum);
  		  fillVMData();
    	}    	        	
    } 
    
    public void MBVMClick(int position) {	    	    	
    	vm_box_cursor.moveToPosition(position); 
        String boxnum = vm_box_cursor.getString(0);
        int boxPos = 0;
    	for (int i = 0; i < boxArray.size(); i++) {
    		String curBox = boxArray.get(i);
    		if (curBox.contentEquals(boxnum)){
    			boxPos = i;
    			break;
    		}
    	}  
    	
    	if (boxPos >= 0) {
    	  doVMDial(boxArray.get(boxPos),pinArray.get(boxPos));
    	}    	        	
    }     
    
    
    public void setVMBoxes (boolean shouldRegister) { 
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	prefs.edit().putInt("numboxes",boxArray.size()).commit();
    	for (int i = 0; i < boxArray.size(); i++) {
    		prefs.edit().putString("box"+i,boxArray.get(i)).commit();
    		try {
				prefs.edit().putString("pin"+i,SimpleCrypto.encrypt(getString(R.string.vs_enc_pw), pinArray.get(i))).commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
    	} 
    	if (shouldRegister){
    	  if ((prefs.getString(getBaseContext().getString(R.string.pref_regid), "").length()>0)&&(prefs.getBoolean(getBaseContext().getString(R.string.pref_vm_notify_alert), false))) {
    	    ServerUtilities.register(getBaseContext(),prefs.getString(getBaseContext().getString(R.string.pref_regid), ""),prefs.getString(getBaseContext().getString(R.string.pref_regid), ""));
    	  }
    	}
    }    
    
    public void getVMBoxes () {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	int max = prefs.getInt("numboxes",0);
    	boxArray.clear();
    	pinArray.clear();
    	for (int i = 0; i < max; i++) {
    		boxArray.add(prefs.getString("box"+i,""));
    		try {
				pinArray.add(SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),prefs.getString("pin"+i,"")));
			} catch (Exception e) {				 
				e.printStackTrace();
			}
    	}
    }
    
    public String getVMArrBoxStr () {
    	String URLStr = "";    	
    	for (int i = 0; i < boxArray.size(); i++) {
    		if (URLStr.length()>0) {URLStr += ",";}
    		URLStr += boxArray.get(i);    		
    	}
    	return URLStr;
    }
    
    @Override
    public void onResume() {
     super.onResume();
     mDbHelper = new VMDBProvider(this);
     mDbHelper.open();         
	    mProgressDialog = new ProgressDialog(getParent());	    
	    mProgressDialog.setMessage("Loading Voicemail");
	    mProgressDialog.setIndeterminate(true);
	    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	    mProgressDialog.setCancelable(true);     
     getVMBoxes();
     if (com.voiceshot.vsdialer.ServerUtilities.fromVMChild) {    	 
       com.voiceshot.vsdialer.ServerUtilities.fromVMChild = false;
       fillVMData();	     	 
     } else {    	 	
       JSONTask jsonTask = new JSONTask(vm_new_box_activity.this,mDbHelper);
       jsonTask.execute("");
     }
    }  
    
    private void fillVMData() {	    	 	    		    	
   	   setContentView(R.layout.vmboxsettings);		
   	   list = (PullToRefreshListView)findViewById(R.id.vm_box_list);
   	   vm_box_cursor = mDbHelper.fetchMailboxes(boxArray);
   	   startManagingCursor(vm_box_cursor);
       String[] columns = new String[] { "_id", "NewMail","SavedMail"};
       int[] to = new int[] { R.id.num_box_text, R.id.num_box_new, R.id.num_box_save };		
   	   SimpleCursorAdapter mAdapter = new SimpleCursorAdapter(this, R.layout.vm_box_row, vm_box_cursor, columns, to);		
   	   list.setAdapter(mAdapter);        	
   	   registerForContextMenu(list);   	 
   	   list.setOnRefreshListener(new OnRefreshListener() {
   	    
   	    @Override
   	    public void onRefresh() {
   	        // Your code to refresh the list contents
   	        
   	        // ...
   	        
   	        // Make sure you call listView.onRefreshComplete()
   	        // when the loading is done. This can be done from here or any
   	        // other place, like on a broadcast receive from your loading
   	        // service or the onPostExecute of your AsyncTask.
   	       JSONTask jsonTask = new JSONTask(vm_new_box_activity.this,mDbHelper);
   	       jsonTask.execute("");
   	    	
   	    }
   	});   	   
    }    
    
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {	  
    	int position = list.getPositionForView(v)-1;	
        String boxnum = vm_box_cursor.getString(0);			    
	  //  menu.setHeaderTitle("Box " +boxnum);
        menu.add(Menu.NONE, DIAL_VM_BOX_MENU, DIAL_VM_BOX_MENU, "Call voicemail box");
	    menu.add(Menu.NONE, DELETE_VM_BOX_MENU, DELETE_VM_BOX_MENU, "Remove box");
	} 
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	  vm_box_cursor.moveToPosition(info.position-1); 	    	
      final String boxnum = vm_box_cursor.getString(0);
      if (item.getItemId()==DIAL_VM_BOX_MENU) {
    	  MBVMClick(info.position-1);
      } else {
        int tmpPos = -1;
	    for (int i = 0; i < boxArray.size(); i++) {
		  String curBox = boxArray.get(i);
		  if (curBox.contentEquals(boxnum)){
			tmpPos = i;			
			break;
		  }
	    }
	    final int boxPos = tmpPos;
	    if (boxPos >= 0) { 
          AlertDialog.Builder alert = new AlertDialog.Builder(
        		getParent());
          alert.setTitle("Confirm");
          alert.setMessage("Are you sure you want to remove Box "+boxnum+" from this device?");
          alert.setPositiveButton("YES", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
      		  boxArray.remove(boxPos);
    		  pinArray.remove(boxPos);
    		  setVMBoxes(true);       		  
    		  mDbHelper.deleteBoxEntry(boxnum);
    		  fillVMData();                      
    		  
              dialog.dismiss();

            }
          });
          alert.setNegativeButton("NO", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          });

          alert.show();      
	    } 	 
      }
	  return true;
	}
	
    public void doVMDial(String vmBox,String vmPIN){ 		
		String vsNumber = "";
			  		  	
	  	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());	  	
	  	vsNumber = ServerUtilities.onlyTheNumbers(prefs.getString(getString(R.string.vs_phone_preferences),""));
	  	 if((vsNumber.length() == 10) && (vmPIN.length() > 0) && (vmBox.length() > 0) ){ 
	      try {
	    	Log.i(TAG,PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO_VM+vmBox+PHONE_DIAL_ACTION_THREE + vmPIN + PHONE_DIAL_ACTION_FOUR);
	        //startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO_VM+vmBox+PHONE_DIAL_ACTION_THREE + vmPIN + PHONE_DIAL_ACTION_FOUR)));
	    	new MyTask().execute(vmBox,vmPIN);

	      } catch (Exception e) {
	        e.printStackTrace();
	      }
	    }
	    else {
		  if (vsNumber.length() != 10) {
		    	  Toast.makeText(getApplicationContext(), "Phone number must be 10 digits", Toast.LENGTH_LONG).show(); 
		  }
	      else if (vmPIN.length() == 0){
	    	  Toast.makeText(getApplicationContext(), "Voicemail PIN cannot be blank!", Toast.LENGTH_LONG).show(); 
	      }
	      else {
	    	  Toast.makeText(getApplicationContext(), "Voiceshot number must be 10 digits", Toast.LENGTH_LONG).show();
	      }
	      }
	}	
    
    
    private class MyTask extends AsyncTask<String, Void, String> 
    {
        @Override
        protected String doInBackground(String... params) 
        {
          Map<String, String> param = new HashMap<String, String>();
                	param.put("RegID", URLEncoder.encode(getRegID(getBaseContext())));
                	param.put("num", URLEncoder.encode(getSharedPhone(getBaseContext())));
                	param.put("BoxNumber", URLEncoder.encode(params[0]));
                	param.put("BoxPIN", URLEncoder.encode(params[1]));
                	param.put("nick", URLEncoder.encode(getNickName(getBaseContext()))); 
                	param.put("Type", URLEncoder.encode("1"));                 	
                	param.put("pin", URLEncoder.encode(getSharedPIN(getBaseContext())));            	
                	try {
    					return ServerUtilities.post("https://api.voiceshot.com/gcm/AppDialer.asp",param);
    				} catch (IOException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    					return "";
    				}          
        }

        @Override
        protected void onPostExecute(String result) 
        {
          processValue(result);
        }
    }  	    
      
    private void processValue(String result){
         if (result.equals("")||result.equals("notfound")) {
        	 Toast.makeText(getApplicationContext(), "Unable to call.  Go to settings and tap Configure App.", Toast.LENGTH_LONG).show();	 
         } else {
    	   startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+getDNIS(getBaseContext())+PHONE_DIAL_ACTION_TWO+result+PHONE_DIAL_ACTION_FOUR)));
         }	    	
    }    
    
    private class JSONTask extends AsyncTask<String, Integer, String> {

        private Context context;        
      //  private Activity activity;
        private VMDBProvider mDbHelper;

        public JSONTask(Context context,VMDBProvider vmd) {
            this.context = context;        
        //    this.activity = a;
            this.mDbHelper = vmd;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            // take CPU lock to prevent CPU from going off if the user 
            // presses the power button during download        	
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                 getClass().getName());
            wl.acquire();

	    	InputStream inputStream = null;
	    	String result = null;            
            try {  
            	   try{    
        	    	DefaultHttpClient   httpclient = new DefaultHttpClient(new BasicHttpParams());
        	    	Log.d(TAG,"https://api.voiceshot.com/gcm/vmGetList.asp?regid="+URLEncoder.encode(getRegID(context))+"&ph="+URLEncoder.encode(getSharedPhone(context))+"&nick="+URLEncoder.encode(getNickName(context))+"&box="+URLEncoder.encode(getVMArrBoxStr().replaceAll("\\s+","")));
        	    	HttpGet httppost = new HttpGet("https://api.voiceshot.com/gcm/vmGetList.asp?regid="+URLEncoder.encode(getRegID(context))+"&ph="+URLEncoder.encode(getSharedPhone(context))+"&nick="+URLEncoder.encode(getNickName(context))+"&box="+URLEncoder.encode(getVMArrBoxStr().replaceAll("\\s+","")));

        	    	try {
        	    	    HttpResponse response = httpclient.execute(httppost);           
        	    	    HttpEntity entity = response.getEntity();

        	    	    inputStream = entity.getContent();
        	    	    // json is UTF-8 by default
        	    	    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
        	    	    StringBuilder sb = new StringBuilder();

        	    	    String line = null;
        	    	    while ((line = reader.readLine()) != null)
        	    	    {
        	    	        sb.append(line);
        	    	    }
        	    	    result = sb.toString();
        	    	} catch (Exception e) { 
        	    	    // Oops
        	    	}  
                   }
        	    	finally {
        	    	    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
        	       }	  
        	    	mDbHelper.deleteALLVMEntry();
        	    	try {
        	    		Log.d(TAG,result);
        				JSONObject jObject = new JSONObject(result);
        				JSONArray items = jObject.getJSONArray("mailboxes");
        				for (int i = 0; i < items.length(); i++) {
        					JSONObject mailboxObject = items.getJSONObject(i);
        					JSONArray newItems = mailboxObject.getJSONArray("newMessages");
        					Log.d(TAG, mailboxObject.getString("box")+ " new messages(" +  mailboxObject.getString("newCount")+") saved messages("+ mailboxObject.getString("savedCount")+")");
        					for (int j = 0; j < newItems.length(); j++) {
        						JSONObject newMsgObj = newItems.getJSONObject(j);
        						mDbHelper.createVMEntry(newMsgObj.getString("dateandtime"),newMsgObj.getString("callerid"),newMsgObj.getString("duration")+" secs",mailboxObject.getString("box"),newMsgObj.getString("uniqueid"),1);
        					}
        					JSONArray savedItems = mailboxObject.getJSONArray("savedMessages");					
        					for (int j = 0; j < savedItems.length(); j++) {
        						JSONObject savedMsgObj = savedItems.getJSONObject(j);
        						mDbHelper.createVMEntry(savedMsgObj.getString("dateandtime"),savedMsgObj.getString("callerid"),savedMsgObj.getString("duration")+" secs",mailboxObject.getString("box"),savedMsgObj.getString("uniqueid"),0);
        					}					
        					
        				}
        			} catch (JSONException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        	    	
            } finally {
                wl.release();
            }
            return null;
        }
            
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.setMessage("Retreiving Voicemail");
            mProgressDialog.setProgress(0);
            mProgressDialog.show();            
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
        	fillVMData();
        	list.onRefreshComplete();
            mProgressDialog.dismiss();            
            if (result != null){ Toast.makeText(context,"Unable to download", Toast.LENGTH_LONG).show();}
            if (notifyVMbox !="") {
            	Intent VMBoxIntent = new Intent(getParent(), boxActivity.class);
            	VMBoxIntent.putExtra("box", notifyVMbox);
            	VMBoxIntent.putExtra("isNew", 1);   
            	notifyVMbox = "";
                TabGroupActivity parentActivity = (TabGroupActivity)getParent();
                parentActivity.startChildActivity("NewActivity", VMBoxIntent);            	
            }
            
        }        
    }    
         	
}
