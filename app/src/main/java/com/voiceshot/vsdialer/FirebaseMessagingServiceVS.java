package com.voiceshot.vsdialer;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import static com.voiceshot.vsdialer.CommonUtilities.showFor;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.setRegID;
import static com.voiceshot.vsdialer.ServerUtilities.delVMBox;
import static com.voiceshot.vsdialer.ServerUtilities.notifyVMbox;

import static com.voiceshot.vsdialer.CommonUtilities.TAG;

public class FirebaseMessagingServiceVS extends FirebaseMessagingService {
    private Handler mMainThreadHandler;
    public FirebaseMessagingServiceVS() {
        mMainThreadHandler = new Handler();
    }

    private class DisplayToast implements Runnable{
        String mText;
        Context mContext;
        int duration;
        int screen_pos;

        public DisplayToast(String text, Context context,int _duration, int _screen_pos){
            mText = text;
            mContext = context;
            duration = _duration;
            screen_pos = _screen_pos;
        }

        public void run(){
            Toast myToast = Toast.makeText(mContext, mText, Toast.LENGTH_LONG);
            if (screen_pos != -1) {
                myToast.setGravity(screen_pos, 0, 0);
            }
            showFor(myToast,duration);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Context context = getApplicationContext();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String message = remoteMessage.getData().get("message"); // intent.getStringExtra("message") ;
            try {
                String tmp = message.substring(0,  Math.min(message.length(), 5));
                if (tmp.equalsIgnoreCase("mt:1,")) {
                    message = message.substring(5, message.length());
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    int iCall_Delay = Integer.parseInt(prefs.getString("prefCallDelay", "24000"));
                    int toast_pos = Integer.parseInt(prefs.getString("prefToastPos", "-1"));
                    SharedPreferences.Editor edit = prefs.edit();
                    String aStr = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                    edit.putString("last_gcm_time",aStr);
                    edit.commit();

                    mMainThreadHandler.post(new DisplayToast(message,getApplicationContext(),iCall_Delay,toast_pos));
                }
                else if (tmp.equalsIgnoreCase("mt:2,")) {
                    message = message.substring(5, message.length());
                    String [] splitArray = message.split("\\|\\|");
                    String dateandtime = splitArray[0];
                    String phonenumber = splitArray[1];
                    String duration    = splitArray[2];
                    String box         = splitArray[3];
                    String filename    = splitArray[4]; // is uniqueid from the site.
                    notifyVMbox        = box;
                    VMDBProvider mDbHelper = new VMDBProvider(context);
                    mDbHelper.open();
                    mDbHelper.createVMEntry(dateandtime, phonenumber, duration, box, filename,1);

                    if (applicationInForeground()){
                        Intent i = new Intent(context,DialNumbers.class);
                        i.putExtra("which_tab","vm_tab");
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);
                    }
                    String tmpMessage = phonenumber;
                    if (phonenumber.isEmpty()) {tmpMessage = "Private Caller";}
                    generateNotification(context, "Voicemail received from "+tmpMessage, box);
                } else if (tmp.equalsIgnoreCase("mt:3,")) {
                    message = message.substring(5, message.length());
                    message = message.replaceAll("\\s+", "");
                    VMDBProvider mDbHelper = new VMDBProvider(context);
                    mDbHelper.open();
                    mDbHelper.deleteBoxEntry(message);
                    delVMBox(context,message);
                    if (applicationInForeground()){
                        mMainThreadHandler.post(new DisplayToast("Box "+message+ " deleted.",getApplicationContext(),6000,-1));
                        Intent i = new Intent(context,DialNumbers.class);
                        i.putExtra("which_tab","vm_tab");
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);
                    }
                }
            } catch (Exception e){
                mMainThreadHandler.post(new DisplayToast(e.toString(),getApplicationContext(),24000,-1));
            }
        }

    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "***********Refreshed token: ********" + token);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean("PREVIOUSLY_STARTED", false);

        //don't register if the app has just been installed and run and the setup screen is showing
        if (previouslyStarted) {
            Context context = getApplicationContext();
            String oldRegID = getRegID(context);
            setRegID(getApplicationContext(), token);

            ServerUtilities.register(context, token, oldRegID);
        }
    }

    private static void generateNotification(Context context, String message, String inBox) {
        Log.i("FMSVS","generateNotification");
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        String title              = "VoiceShot Voicemail";

        Intent notificationIntent = new Intent(context, DialNumbers.class);
        notificationIntent.putExtra("which_tab","vm_tab");
        notificationIntent.putExtra("which_box",inBox);
        notificationIntent.setAction("android.intent.action.MAIN");
        notificationIntent.addCategory("android.intent.category.LAUNCHER");

        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        Notification notification = new Notification.Builder(context)
                .setContentIntent(intent)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.stat_notify_voicemail)
                .build();

        notification.flags    |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);
    }

    private boolean applicationInForeground() {
    	boolean isActivityFound = false;
        ActivityManager activityManager = (ActivityManager) getSystemService( Context.ACTIVITY_SERVICE );
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for(ActivityManager.RunningAppProcessInfo appProcess : appProcesses){
            if(appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND){
                Log.i("Foreground App", appProcess.processName);
               if (appProcess.processName.equalsIgnoreCase(getPackageName())){
            	   isActivityFound = true;
               }
            }
        }

        return isActivityFound;
    }
}
