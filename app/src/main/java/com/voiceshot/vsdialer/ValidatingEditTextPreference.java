package com.voiceshot.vsdialer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;
import android.util.Log;
import android.text.TextWatcher;

import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.ServerUtilities.getAllDNIS;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ValidatingEditTextPreference extends EditTextPreference {
	private static InstantAutoComplete mEditText = null;	
	//String[] validWords = new String[]{"8056246645", "8667590582", "8667863885", "8667883227","8778324558"};
	String[] validDNIS = null;
	private static AttributeSet _attrs = null;
	private static int _defStyleAttr;
	private static Context _context = null;

    public ValidatingEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mEditText = new InstantAutoComplete(context, attrs,defStyleAttr);
        mEditText.setThreshold(0);        
    
    }

    public ValidatingEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        _attrs = attrs;
        _context = context;
    }

    public ValidatingEditTextPreference(Context context) {
        super(context);

       _context = context;
    }
    

    
    @Override
    protected void onBindDialogView(View view) {
    	ArrayAdapter<String> adapter = null;
    	validDNIS = getAllDNIS(getContext()).split("\\|");
    	if (_attrs != null) {
            mEditText = new InstantAutoComplete(_context,_attrs);               		
    	} else {
            mEditText = new InstantAutoComplete(_context);                		
    	}
    	mEditText.setThreshold(0);
    	mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
    	int maxLengthofEditText = 10;    
    	mEditText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLengthofEditText)});
        if (validDNIS.length > 1) {
        	adapter = new burtuAdapteris(getContext(), android.R.layout.simple_dropdown_item_1line, validDNIS);
        }
        mEditText.setAdapter(adapter);     	
        InstantAutoComplete editText = mEditText;
        editText.setText(getText());        
        ViewParent oldParent = editText.getParent();
        if (oldParent != view) {
            if (oldParent != null) {
                ((ViewGroup) oldParent).removeView(editText);
            }
            onAddEditTextToDialogView(view, editText);
        }
        mEditText.setSelection(mEditText.getText().length());


        
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            String value = mEditText.getText().toString();
            if (callChangeListener(value)) {
                setText(value);
                SharedPreferences pref = getContext().getSharedPreferences(getContext().getPackageName() + "_preferences", Context.MODE_PRIVATE); 
                String aStr = pref.getString(getContext().getString(R.string.pref_dnis_arr),"");  
                String newStr = "";
                if (!aStr.contains(value)) {
                  if (aStr.length()==0) {
                	  newStr = value;
                  } else {
                	  newStr = aStr + "|" + value;  
                  }
                  SharedPreferences.Editor edit = pref.edit();
                  edit.putString(getContext().getString(R.string.pref_dnis_arr),newStr);
                  edit.commit();
                }
            }
        }
    }  

   @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        AlertDialog dlg = (AlertDialog)getDialog();
        View positiveButton = dlg.getButton(DialogInterface.BUTTON_POSITIVE);
        mEditText.setError(null);                
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPositiveButtonClicked(v);
            }
        }); 
        mEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            	mEditText.setView();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            	 
            }
        });        
      /*  mEditText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            	mEditText.showDropDown();     
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            	mEditText.showDropDown();
            }        	
        });
       mEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              mEditText.showDropDown();
              //      Toast.makeText(getContext(), "Position clicked: " + position, Toast.LENGTH_SHORT).show();
            }
      });*/

    }

    private void onPositiveButtonClicked(View v) {
        String errorMessage = onValidate(mEditText.getText().toString());
        if (errorMessage == null)
        {
        	mEditText.setError(null);
            onClick(getDialog(),DialogInterface.BUTTON_POSITIVE);
            getDialog().dismiss();
        } else {
        	mEditText.setError(errorMessage);
            return; // return WITHOUT dismissing the dialog.
        }
    }

    /***
     * Called to validate contents of the edit text.
     *
     * Return null to indicate success, or return a validation error message to display on the edit text.
     *
     * @param text The text to validate.
     * @return An error message, or null if the value passes validation.
     */
    public String onValidate(String text)
    {
        try {
            if (text.length()==10) {
            return null;
            }
            else {
            	return "Number must be 10 digits long."; 	
            }
        } catch (Exception e)
        {
            return "Number must be 10 digits long.";
        }
    }
    
    public class InstantAutoComplete extends AutoCompleteTextView {
    	private boolean enoughFilter = false;

        public InstantAutoComplete(Context context) {
            super(context);
            
        }

        public InstantAutoComplete(Context arg0, AttributeSet arg1) {
            super(arg0, arg1);
                      
        }

        public InstantAutoComplete(Context arg0, AttributeSet arg1, int arg2) {
            super(arg0, arg1, arg2);
                    
            
        }
        
        public void setView() {
          if ((!enoughFilter) && (getAdapter() != null)) {
              try {
            	  enoughFilter = true;
          		//	Method setDropDownAlwaysVisible = ((AutoCompleteTextView)mEditText).getClass().getMethod("setDropDownAlwaysVisible",AutoCompleteTextView.class);
          		//	setDropDownAlwaysVisible.invoke(((AutoCompleteTextView)mEditText), true);
                  	Method[] methods = this.getClass().getMethods();
                  	for (Method m : methods){
                  		//Log.i(TAG,m.getName());
                  	    if (m.getName().equals("setDropDownAlwaysVisible")){
                  	        m.invoke(this, true);
                  	        break;
                  	    }        		
                  	}
          		} catch (SecurityException e) {
          			// TODO Auto-generated catch block
          			e.printStackTrace();
          		} /*catch (NoSuchMethodException e) {

          			e.printStackTrace();
          		}*/ catch (IllegalArgumentException e) {
          			// TODO Auto-generated catch block
          			e.printStackTrace();
          		} catch (IllegalAccessException e) {
          			e.printStackTrace();
          		} catch (InvocationTargetException e) {
          			// TODO Auto-generated catch block
          			e.printStackTrace();
          		}  
              this.showDropDown();
          }
        }

        @Override
        public boolean enoughToFilter() {
            return enoughFilter;
        }

        @Override
        protected void onFocusChanged(boolean focused, int direction,
                Rect previouslyFocusedRect) {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
         /*   if (focused && getAdapter() != null) {
                performFiltering(getText(), 0);
            }*/
        }
        
      /*  @Override
        public boolean onKeyPreIme(int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.ACTION_DOWN) {
            	    performFiltering(getText(), 0);
                    return true;
            }
            return super.dispatchKeyEvent(event);
        }   */
        
        @Override
        public boolean onTouchEvent(MotionEvent event) {
          if (event.getAction()==MotionEvent.ACTION_DOWN){
        	  setView();
        	  //performFiltering(getText(), 0);
        	  
        	  return true;
          }
          return super.onTouchEvent(event);
        }
        
      
    }    
    
    public class burtuAdapteris extends ArrayAdapter<String> implements Filterable {

        String[] _items ;
        String[] orig ;

        public burtuAdapteris(Context context, int resource, String[] items) {
            super(context, resource, items);    
            orig = items.clone();
        }

        @Override
        public int getCount() {
            if (_items != null)
                return _items.length;
            else
                return 0;
        }

        @Override
        public String getItem(int arg0){         
            return _items[arg0];
        }
        
        @Override
        public boolean isEnabled (int position) {
           return true;
        }        


       @Override

       public Filter getFilter() {
           Filter filter = new Filter() {
               @Override
               protected FilterResults performFiltering(CharSequence constraint) {


                   FilterResults oReturn = new FilterResults();



                       _items = null;
                       _items = orig.clone();

                       oReturn.values = _items;
                       oReturn.count = _items.length;

                   return oReturn;
               }


               @SuppressWarnings("unchecked")
               @Override
               protected void publishResults(CharSequence constraint, FilterResults results) {
                   if(results != null && results.count > 0) {
                         notifyDataSetChanged();
                         }
                         else {
                             notifyDataSetInvalidated();
                         }

               }

             };

           return filter;

       }


  }    
}


