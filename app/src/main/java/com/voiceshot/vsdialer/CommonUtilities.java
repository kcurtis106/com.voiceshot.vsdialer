package com.voiceshot.vsdialer;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;




/**
 * Helper class providing methods and constants common to other classes in the
 * app.
 */
final class ContactResult {
    private final String contactName;
    private final String contactType;
    private final String contactID;
    private final Bitmap bitMap;

    public ContactResult(String contactID, String contactName, String contactType,Bitmap contactBitMap) {
        this.contactName = contactName;
        this.contactType = contactType;
        this.contactID   = contactID;
        this.bitMap      = contactBitMap;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactType() {
        return contactType;
    }
    
    public String getContactID() {
        return contactID;
    }    
    
    public Bitmap getBitmap() {
        return bitMap;
    }     
}	

public final class CommonUtilities {
	


    /**
     * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
     */
    static final String SERVER_URL = "https://api.voiceshot.com/gcm";

    /**
     * Google API project id registered to use GCM.
     */
    static final String SENDER_ID = "689067616726";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "com.voiceshot.vsdialer";

    /**
     * Intent used to display a message in the screen.
     */
    static final String DISPLAY_MESSAGE_ACTION =
            "com.voiceshot.vsdialer.DISPLAY_MESSAGE";
    
    static final String DISPLAY_REFRESH_PREF =
            "com.voiceshot.vsdialer.REFRESH_PREF";
    
    static final String REGISTRATION_ACTION =
            "com.voiceshot.vsdialer.REGISTRATION_ACTION";
    
    static final String UNREGISTER_ACTION =
            "com.voiceshot.vsdialer.UNREGISTER_ACTION";    
    
    static final String PHONE_RING_MESSAGE_ACTION =
            "com.voiceshot.vsdialer.PHONE_RING_MESSAGE_ACTION";   
    static final String REMOVE_BOX_ACTION =
            "com.voiceshot.vsdialer.REMOVE_BOX_ACTION"; 
    static final String PHONE_DIAL_ACTION_ONE = "tel:";
    static final String PHONE_DIAL_ACTION_TWO = ",,%23,*2";
    static final String PHONE_DIAL_ACTION_TWO_VM = ",,%23,*1";
    static final String PHONE_DIAL_ACTION_THREE = "%2A,,";
    static final String PHONE_DIAL_ACTION_FOUR = "%23";
    
    static final String DISPLAY_REFRESH_NEW_VM = "com.voiceshot.dialnumbers.vm_box_activity.refresh";
    
    /**
     * Intent's extra that contains the message to be displayed.
     */
    
    static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
    
    static void sendRemoveBox(Context context) {
        Intent intent = new Intent(REMOVE_BOX_ACTION);        
        context.sendBroadcast(intent);
    }    
    
    static void sendReg(Context context) {
        Intent intent = new Intent(REGISTRATION_ACTION);        
        context.sendBroadcast(intent);
    }
    
    static void sendUnreg(Context context) {
        Intent intent = new Intent(UNREGISTER_ACTION);        
        context.sendBroadcast(intent);
    }    
    
    static void displayPhoneRingMessage(Context context, String message) {
        Intent intent = new Intent(PHONE_RING_MESSAGE_ACTION);
        context.sendBroadcast(intent);
 	
    }  
    
    static void refreshPref(Context context) {
        Intent intent = new Intent(DISPLAY_REFRESH_PREF);        
        context.sendBroadcast(intent);
    }    
    
	public static void showFor(final Toast aToast, final long durationInMilliseconds) {

        aToast.setDuration(Toast.LENGTH_SHORT);

        Thread t = new Thread() {
        long timeElapsed = 0l;

         public void run() {
          try {
             while (timeElapsed <= durationInMilliseconds) {
                  long start = System.currentTimeMillis();
                  aToast.show();
                  sleep(1750);
                  timeElapsed += System.currentTimeMillis() - start;
             }
          } catch (InterruptedException e) {
             //  Log.e(TAG, e.toString());
             }
          }
        };
       t.start();
    } 
	
	public static ContactResult getContactName(Context context, String phoneNumber) {
	    String contactName = null;
	    String phoneType   = null;
	    String contactID   = null;
	    Bitmap photo = null;
	    try {
	      ContentResolver cr = context.getContentResolver();
	      Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(phoneNumber));	    //PhoneLookup.CONTENT_FILTER_URI  
	      Cursor cursor = cr.query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.TYPE,ContactsContract.CommonDataKinds.Phone._ID,ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI}, null, null, null);
	      if (cursor == null) {
	          return null;
	      }

   	      if(cursor.moveToFirst()) {
   	    	
	          contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));	        
	          phoneType   = (String) Phone.getTypeLabel(context.getResources(),cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)),"");	        
	          contactID   = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
	          String image_uri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
	            	  Uri thumbUri= Uri.parse(image_uri);
	            	  AssetFileDescriptor afd = null;
	            	  try {
	                      afd = context.getContentResolver().openAssetFileDescriptor(thumbUri, "r");
	                      FileDescriptor fileDescriptor = afd.getFileDescriptor();              
	                      photo = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, null);        		  
	            	  }  catch (FileNotFoundException e) {
	            		  //
	            	  }         
	      }

	      if(cursor != null && !cursor.isClosed()) {
	          cursor.close();
	      }	     

	    
	    	
	    } catch (Exception e) {
	    	// Log.e(TAG, e.toString());	
	    }
	    
	    return new ContactResult(contactID,contactName,phoneType,photo);
	}	
	
    public static String getPhotoUri(Context context,String contactId) {
        ContentResolver contentResolver = context.getContentResolver();

        try {
            Cursor cursor = contentResolver
                    .query(ContactsContract.Data.CONTENT_URI,
                            null,
                            ContactsContract.Data.CONTACT_ID
                                    + "="
                                    + contactId
                                  /*  + " AND "

                                    + ContactsContract.Data.MIMETYPE
                                    + "='"
                                    + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                    + "'" */, null, null);

            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    return null; // no photo
                } else {
                	return cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
                }
            } else {
                return null; // error in cursor process
            }

        } catch (Exception e) {
           // e.printStackTrace();
            return null;
        }
        

      //  Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
      //  return Uri.withAppendedPath(person,ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }	
    
    
            
}
