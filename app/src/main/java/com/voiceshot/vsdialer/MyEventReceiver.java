package com.voiceshot.vsdialer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.setRegID;

public class MyEventReceiver extends BroadcastReceiver
{     
    Context mycontext;
    @Override public void onReceive(Context context, Intent intent)
    {
       if(Intent.ACTION_PACKAGE_REPLACED.equals(intent.getAction())) 
       {   if(intent.getData().getSchemeSpecificPart().equals(context.getPackageName()))
           {  
    	   		//GCMRegistrar.register(context, SENDER_ID);
               mycontext = context;
               Log.i("dbg", "onReceive: my event receiver");
               /*FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                   @Override
                   public void onComplete(@NonNull Task<InstanceIdResult> task) {
                       if (!task.isSuccessful()) {
                           Log.w(TAG, "MyEventReceiver getInstanceId failed", task.getException());
                           return;
                       }

                       // Get new Instance ID token
                       String token = task.getResult().getToken();
                       Log.d(TAG, "MyEventReceiver vs Token " + token);

                       String oldRegID = getRegID(mycontext);
                       setRegID(mycontext,token);
                       SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mycontext);
                       prefs.edit().putString(Resources.getSystem().getString(R.string.pref_regid),token).commit();
                       ServerUtilities.register(mycontext, token,oldRegID);
                   }
               });*/

           }
       }
    }      
}
