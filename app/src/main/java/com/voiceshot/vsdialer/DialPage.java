package com.voiceshot.vsdialer;

import com.voiceshot.vsdialer.R;

import static android.Manifest.*;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_ONE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_TWO;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;

import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_THREE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_FOUR;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPhone;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.getNickName;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPIN;
import static com.voiceshot.vsdialer.ServerUtilities.getDNIS;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MotionEvent;
import java.util.Map;

import android.content.pm.PackageManager;
import android.Manifest;
import android.support.v4.app.ActivityCompat;

public class DialPage extends Activity {


	private MyListener listener = null;
	private Boolean MyListenerIsRegistered = false;
	private AudioManager am;
	private ToneGenerator mp1;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final int PERMISSIONS_REQUEST_CALL_PHONE = 101;

	private class MyTask extends AsyncTask<String, Void, String>
	{
		@Override
		protected String doInBackground(String... params)
		{
			Map<String, String> param = new HashMap<String, String>();
			param.put("RegID", URLEncoder.encode(getRegID(getBaseContext())));
			param.put("num", URLEncoder.encode(getSharedPhone(getBaseContext())));
			param.put("target", URLEncoder.encode(params[0]));
			param.put("nick", URLEncoder.encode(getNickName(getBaseContext())));
			param.put("Type", URLEncoder.encode("0"));
			param.put("pin", URLEncoder.encode(getSharedPIN(getBaseContext())));
			try {
				return ServerUtilities.post("https://api.voiceshot.com/gcm/AppDialer.asp",param);
			} catch (IOException e) {
				e.printStackTrace();
				return "";
			}
		}

		@Override
		protected void onPostExecute(String result)
		{
			processValue(result);
		}
	}

	private void processValue(String result) {
		if (result.equals("")||result.equals("notfound")) {
			Toast.makeText(getApplicationContext(), "Unable to call.  Go to settings and tap Configure App.", Toast.LENGTH_LONG).show();
		} else {
			Log.i(TAG,"Dial result : "+result);
			startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+getDNIS(getBaseContext())+PHONE_DIAL_ACTION_TWO+result+PHONE_DIAL_ACTION_FOUR)));
		}
	}

	/*@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, view, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.paste_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
	 // Log.i("paste","Paste");
	  ClipboardManager cManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
	  if ((cManager.hasPrimaryClip())) {
	 // Log.i("paste", cManager.getPrimaryClip().getItemAt(0).getText());
	   if ((cManager.getPrimaryClipDescription().hasMimeType(android.content.ClipDescription.MIMETYPE_TEXT_PLAIN))) {
		  int tmpLen = ServerUtilities.onlyTheNumbers(cManager.getPrimaryClip().getItemAt(0).getText()).length();
		  if ((tmpLen > 0) && (tmpLen <= 10)) {
			  ((EditText)findViewById(R.id.text_number)).append(ServerUtilities.onlyTheNumbers(cManager.getPrimaryClip().getItemAt(0).getText()));
		  } else {
			  Toast.makeText(getApplicationContext(), "Phone number to paste not 10 digits long", Toast.LENGTH_LONG).show();
		  }
	   } else {
		   Toast.makeText(getApplicationContext(), "Content to paste is not text", Toast.LENGTH_LONG).show();
	   }
	  } else {
	   Toast.makeText(getApplicationContext(), "Clipboard empty", Toast.LENGTH_LONG).show();
	  }

	  return true;
	}*/


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.scalingdialnumberlayout);
		setContentView(R.layout.flatbuttonlayout);
		listener = new MyListener();
		EditText edtView=(EditText)findViewById(R.id.text_number);
		// registerForContextMenu(edtView);
		// edtView.setInputType(0);
		edtView.setTextIsSelectable(true);
		edtView.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		edtView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.onTouchEvent(event);
				InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				if (imm != null) {
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				}
				return true;
			}
		});
		//edtView.setFocusableInTouchMode(false);
	    /*edtView.setOnLongClickListener(new OnLongClickListener() {
			 public boolean onLongClick(View v) {
				 // Perform action on click
				   ClipboardManager cManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				   if ((cManager.hasPrimaryClip())) {
				  // Log.i("paste", cManager.getPrimaryClip().getItemAt(0).getText());
					   if ((cManager.getPrimaryClipDescription().hasMimeType(android.content.ClipDescription.MIMETYPE_TEXT_PLAIN))) {
						  int tmpLen = ServerUtilities.onlyTheNumbers(cManager.getPrimaryClip().getItemAt(0).getText()).length();
						  if ((tmpLen > 0) && (tmpLen <= 10)) {
							  ((EditText)v).setText(ServerUtilities.onlyTheNumbers(cManager.getPrimaryClip().getItemAt(0).getText()));
						  } else {
							  Toast.makeText(getApplicationContext(), "Phone number to paste not 10 digits long", Toast.LENGTH_LONG).show();
						  }
					   } else {
						   Toast.makeText(getApplicationContext(), "Content to paste is not text", Toast.LENGTH_LONG).show();
					   }
				   } else {
					   Toast.makeText(getApplicationContext(), "Clipboard empty", Toast.LENGTH_LONG).show();
				   }
				 //  ClipData cData = ClipData.newPlainText("Paste", cManager.getPrimaryClip());
				  // cManager.setPrimaryClip(cData);
				   return true;
			 }
		 });*/
		am = (AudioManager)getSystemService(AUDIO_SERVICE);
		mp1 = new ToneGenerator(AudioManager.STREAM_DTMF,70);


		ImageButton button = (ImageButton) findViewById(R.id.btn_delete);
		button.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {
				// Perform action on click
				setCn("");
				return true; }
		});


		button.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN ) {
					v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
					return false;
				}

				return false;
			}
		});

		// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}


	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			(getParent()).moveTaskToBack(true);
			return true;
		}
		else return super.onKeyDown(keyCode, event);

	}

	public void setCn (String inNumber){
		// vsDialNumber = inNumber;
		inNumber = inNumber.replaceAll( "[^\\d]", "" );
		((EditText) findViewById(R.id.text_number)).setText("",TextView.BufferType.EDITABLE);
		((EditText) findViewById(R.id.text_number)).append(inNumber);
	}


	public void callLogClick (View view){
		getCallLog();
	}

	public void contactsClick (View view) {
		getContacts();
	}

	public void optionsClick (View view) {
		// openOptionsMenu();
		vmboxClick();
	}


	public void getCallLog(){
		//Intent intent = new Intent(this, CallLogList.class);
		//startActivityForResult(intent, DialNumbers.PICK_LOG);
	}

	public void vmboxClick(){
		Intent intent = new Intent(this, VM_Box_Activity.class);
		startActivityForResult(intent,DialNumbers.PICK_VM);
	}

	public void getContacts(){
		// Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        boolean canGetContacts = false;
		if (ContextCompat.checkSelfPermission(this, permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) //getApplicationContext
            ActivityCompat.requestPermissions(getParent(), new String[]{permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
		    //requestPermissions(new String[]{permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        else {
            canGetContacts = true;
        }

        if (canGetContacts) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);

            startActivityForResult(intent, DialNumbers.PICK_CONTACT);
        }
	}

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                getContacts();
            } else {
                Toast.makeText(this, "Please grant VoiceShot dialer permission to display your contacts", Toast.LENGTH_SHORT).show();
            }
        }
    }

	@Override public void onActivityResult(int reqCode, int resultCode, Intent data){
		super.onActivityResult(reqCode, resultCode, data);
		switch(reqCode) {
			case (DialNumbers.PICK_CONTACT):
				if (resultCode == Activity.RESULT_OK)      {
					Uri contactData = data.getData();
					Cursor cur = managedQuery(contactData, null, null, null, null);
					// ContentResolver contect_resolver = getContentResolver();
					if (cur.moveToFirst()) {
						String id = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
						Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,        ContactsContract.CommonDataKinds.Phone._ID +" = "+ id,null, null);
						phones.moveToFirst();
						String cNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						setCn(cNumber);
						id = null;
						phones = null;
					}
					cur = null;
				}
				break;
			case (DialNumbers.PICK_LOG):
				if (resultCode == Activity.RESULT_OK) {
					String cNumber = data.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
					setCn(cNumber);
				}
				else if (resultCode == DialNumbers.RESULT_CONTACT_BUTTON_PUSHED) {
					getContacts();
				}
				else if (resultCode == DialNumbers.RESULT_OPTIONS_BUTTON_PUSHED) {
					vmboxClick();
				}
				break;
			case (DialNumbers.PICK_OPTION):
				if (resultCode == DialNumbers.RESULT_CONTACT_BUTTON_PUSHED) {
					getContacts();
				}
				else if (resultCode == DialNumbers.RESULT_LOG_BUTTON_PUSHED) {
					getCallLog();
				}
				break;
			case (DialNumbers.PICK_VM):
				if (resultCode == DialNumbers.RESULT_CONTACT_BUTTON_PUSHED) {
					getContacts();
				}
				else if (resultCode == DialNumbers.RESULT_LOG_BUTTON_PUSHED) {
					getCallLog();
				}
				break;
		}
	}

	public void doDial(View view){
		String vsNumber = "";
		String ddPIN    = "";
		String PhoneNumber = ((EditText) findViewById(R.id.text_number)).getText().toString();
		PhoneNumber = PhoneNumber.replaceAll( "[^\\d]", "" );
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		vsNumber = ServerUtilities.onlyTheNumbers(prefs.getString(getString(R.string.vs_phone_preferences),""));
		try {
			ddPIN = SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),prefs.getString(getString(R.string.dd_pin_preferences),""));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if(((PhoneNumber.length() == 10) && (ddPIN.length() > 0) && (vsNumber.length() == 10) ) || (PhoneNumber.equalsIgnoreCase("911"))){
			try {
                if (ContextCompat.checkSelfPermission(this, permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) //getApplicationContext
                    ActivityCompat.requestPermissions(getParent(), new String[]{permission.CALL_PHONE}, PERMISSIONS_REQUEST_CALL_PHONE);
                else {
                    if (PhoneNumber.equalsIgnoreCase("911")) {
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE + PhoneNumber)));
                    } else {
                        //startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO+ddPIN+PHONE_DIAL_ACTION_THREE + PhoneNumber + PHONE_DIAL_ACTION_FOUR)));
                        new MyTask().execute(PhoneNumber);
                    }
                }

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			if (PhoneNumber.length() != 10) {
				Toast.makeText(getApplicationContext(), "Phone number must be 10 digits", Toast.LENGTH_LONG).show();
			}
			else if (ddPIN.length() == 0){
				Toast.makeText(getApplicationContext(), "Outbound calling PIN cannot be blank!", Toast.LENGTH_LONG).show();
			}
			else {
				Toast.makeText(getApplicationContext(), "Voiceshot number must be 10 digits", Toast.LENGTH_LONG).show();
			}
		}
	}


	public void addKey (View view) {
		int inKey =  Integer.parseInt(((String) view.getTag()));
		int keyCode = KeyEvent.KEYCODE_0;
		//((EditText) findViewById(R.id.text_number)).append(inKey);
		switch(inKey){
			case 1:  keyCode=KeyEvent.KEYCODE_1;
				break;
			case 2: keyCode=KeyEvent.KEYCODE_2;
				break;
			case 3: keyCode=KeyEvent.KEYCODE_3;
				break;
			case 4: keyCode=KeyEvent.KEYCODE_4;
				break;
			case 5: keyCode=KeyEvent.KEYCODE_5;
				break;
			case 6: keyCode=KeyEvent.KEYCODE_6;
				break;
			case 7: keyCode=KeyEvent.KEYCODE_7;
				break;
			case 8: keyCode=KeyEvent.KEYCODE_8;
				break;
			case 9: keyCode=KeyEvent.KEYCODE_9;
				break;
			case 0: keyCode=KeyEvent.KEYCODE_0;
				break;
			default:
				break;
		}
		KeyEvent event = new KeyEvent(0, 0, 0, keyCode, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
		((EditText) findViewById(R.id.text_number)).dispatchKeyEvent(event);
		int currentRingerMode = am.getRingerMode();
		boolean isDtmfToneEnabled = Settings.System.getInt(getContentResolver(), Settings.System.DTMF_TONE_WHEN_DIALING, 1) != 0;

		if (!((currentRingerMode == AudioManager.RINGER_MODE_SILENT)||(currentRingerMode == AudioManager.RINGER_MODE_VIBRATE))) {
			if (isDtmfToneEnabled) {
				mp1.stopTone();
				mp1.startTone(inKey, 120);
			}
		}
		view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
	}

	public void delKey (View view) {
		KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
		((EditText) findViewById(R.id.text_number)).dispatchKeyEvent(event);
		//AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		//((EditText) findViewById(R.id.text_number)).playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
		//am.playSoundEffect(AudioManager.FX_KEY_CLICK);

		//view.setSoundEffectsEnabled(true);
		view.playSoundEffect(0);
		//view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!MyListenerIsRegistered) {
			registerReceiver(listener, new IntentFilter("com.voiceshot.dialnumbers.setDial"));
			MyListenerIsRegistered = true;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (MyListenerIsRegistered) {
			unregisterReceiver(listener);
			MyListenerIsRegistered = false;
		}
	}

	// Nested 'listener'
	protected class MyListener extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			// No need to check for the action unless the listener will
			// will handle more than one - let's do it anyway
			if (intent.getAction().equals("com.voiceshot.dialnumbers.setDial")) {
				String cNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
				if (cNumber.length() > 0) {
					if (cNumber.endsWith("-1")) {
						getContacts();
					}
					else {
						setCn(cNumber);
					}
				}
			}
		}
	}


}
