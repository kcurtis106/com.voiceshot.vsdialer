package com.voiceshot.vsdialer;

//import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.voiceshot.vsdialer.R;

import static com.voiceshot.vsdialer.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.EXTRA_MESSAGE;
import static com.voiceshot.vsdialer.CommonUtilities.DISPLAY_REFRESH_PREF;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.CommonUtilities.displayPhoneRingMessage;
import static com.voiceshot.vsdialer.CommonUtilities.sendUnreg;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.voiceshot.vsdialer.CommonUtilities.sendReg;
import static com.voiceshot.vsdialer.CommonUtilities.displayMessage;
import static com.voiceshot.vsdialer.CommonUtilities.SENDER_ID;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.getUrlParameters;
import static com.voiceshot.vsdialer.ServerUtilities.setDNIS;
import static com.voiceshot.vsdialer.ServerUtilities.setDDPIN;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPIN;
import static com.voiceshot.vsdialer.ServerUtilities.setVMBoxStr;
import static com.voiceshot.vsdialer.ServerUtilities.setNickName;
import static com.voiceshot.vsdialer.ServerUtilities.setRegID;
import static com.voiceshot.vsdialer.ServerUtilities.setSendVMAlert;
import static com.voiceshot.vsdialer.ServerUtilities.setSendAlert;
import static com.voiceshot.vsdialer.ServerUtilities.setSendAndVMAlert;
import static com.voiceshot.vsdialer.ServerUtilities.setAllDnis;
import static com.voiceshot.vsdialer.ServerUtilities.clearAppData;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPhone;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.voiceshot.vsdialer.ServerUtilities;
import com.voiceshot.vsdialer.ValidatingEditTextPreference;



public class Preferences extends PreferenceActivity implements OnSharedPreferenceChangeListener,OnPreferenceChangeListener { 
	private EncEditTextShowPINDialog mEncPINDialog;
	private String version = BuildConfig.VERSION_NAME;
	AsyncTask<Void, Void, Void> mRegisterTask;
	AsyncTask<String, Void, String> mLoginTask;
	Context thisContext;
	AlertDialog pinDLG;
	AlertDialog alert11;
	View v;
	ProgressDialog pdialog;
	private boolean IgnoreOnce = false;
	private boolean createloginDialog = false;
	
	String[] validWords = new String[]{""};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);          
        setContentView(R.layout.activity_preferences);  
        getActionBar().setDisplayHomeAsUpEnabled(true);
        try {
        	addPreferencesFromResource(R.xml.preferences); 	
        } catch (ClassCastException e) {
        	e.printStackTrace();
        }
        mEncPINDialog = (EncEditTextShowPINDialog) findPreference(getString(R.string.dd_pin_preferences));
        mEncPINDialog.setOnPreferenceChangeListener(this);   
        try {
        mEncPINDialog.setText(SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(getBaseContext())).getString(getString(R.string.dd_pin_preferences),"")));
        } catch (Exception e) { e.printStackTrace();  }
        mEncPINDialog.setCompoundButtonListener(new View.OnClickListener()  
        {			              	     
        	public void onClick(View v) 
        	{             
        		if (mEncPINDialog.isShowPINChecked()) {
        		   mEncPINDialog.setEditTransformationMethod(null);        		
        		}
        		else {
        		   mEncPINDialog.setEditTransformationMethod(PasswordTransformationMethod.getInstance()) ;        			
        		}
        	}  
        } );


        (findPreference(getString(R.string.pref_local_phone))).setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference,
                    Object newValue) {
                if ((newValue.toString().length() == 10)||(newValue.toString().length() == 11)) {
                      //
                      return true;
                 }
                 else{
                	 Toast.makeText(getApplicationContext(), "Number must be 10 digits long.", Toast.LENGTH_LONG).show();
                	 ((myEditTextPreference)(findPreference(getString(R.string.pref_local_phone)))).show();
                     return false;
                 }
          }
        });       
                

        Preference VerPref = findPreference("preference_version");
        VerPref.setSummary(version);
        Preference myPref = (Preference) findPreference(getString(R.string.pref_config_click));        
        myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
            	createUsernamePasswordDialog(); 
            	return true;
            }
        });        
        
        registerReceiver(mHandleRefreshReceiver,  new IntentFilter(DISPLAY_REFRESH_PREF));
        
        for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++){
        	initSummary(getPreferenceScreen().getPreference(i));               
        }         
        thisContext = this;
        
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean("PREVIOUSLY_STARTED", false);
		//previouslyStarted = false;  //debug added by N
        if(!previouslyStarted){
        	//prefs.edit().putBoolean("PREVIOUSLY_STARTED", true).commit();  //commented 3/29/19 by N, should only be set upon registration, not dialog display
        	createUsernamePasswordDialog();        	
        }        
    }
    @Override          
    protected void onResume(){
    	super.onResume();             // Set up a listener whenever a key changes
    	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);     			
    	
    }
    
    @Override          
    protected void onPause() { 
    	super.onPause();             // Unregister the listener whenever a key changes
    	com.voiceshot.vsdialer.ServerUtilities.fromVMChild = false;
    	getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this); 
    }       
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        finish();
    	return true;
    }    
    
    public void createUsernamePasswordDialog() {
    	if (ServerUtilities.onlyTheNumbers(ServerUtilities.getSharedPhone(getBaseContext()))=="") {
    		createloginDialog = true;
    		myEditTextPreference mypref = (myEditTextPreference)findPreference(getString(R.string.pref_local_phone));
    		mypref.show();
    		return;
    	}
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);           
   	    LayoutInflater inflater = this.getLayoutInflater();
   	    v = inflater.inflate(R.layout.sign_in, null);	
   	    builder.setView(v)
   	    // Add action buttons
   	           .setPositiveButton(R.string.sign_in, new DialogInterface.OnClickListener() {
   	               @Override
   	               public void onClick(DialogInterface dialog, int id) {   	            	 
                     //
   	               }
   	           })
   	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
   	               public void onClick(DialogInterface dialog, int id) {       	            	  
   	                  dialog.cancel();   	                  
   	                  alert11 = null;
   	               }
   	           });
   	   alert11 = builder.create();
   	   alert11.setTitle("VoiceShot Username/Password");
   	   alert11.show();   
   	   alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
       {            
           @Override
           public void onClick(View v)
           {
                  // sign in the user ...
           	 String username = ((EditText)alert11.findViewById(R.id.vs_login_username)).getText().toString();
           	 String password = ((EditText)alert11.findViewById(R.id.vs_login_password)).getText().toString();
           	 if ((username.length()==0)||(password.length()==0)) {           		
           		displayMessage(getBaseContext(),"Username/Password cannot be blank.");	 
           	 } else {
           		alert11.hide();               		
           	    DoAppConfig(username,password,ServerUtilities.onlyTheNumbers(ServerUtilities.getSharedPhone(getBaseContext())));
           	 }
                                  
           }
       });   	   
    }
    
    public void createPINDialog() {
   
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);           
   	    LayoutInflater inflater = this.getLayoutInflater();
   	    v = inflater.inflate(R.layout.sign_in_pin, null);	
   	    builder.setView(v)
   	    // Add action buttons
   	           .setPositiveButton(R.string.sign_in_PIN_save, new DialogInterface.OnClickListener() {
   	               @Override
   	               public void onClick(DialogInterface dialog, int id) {
   	            	 if (((EditText)v.findViewById(R.id.vs_login_PIN)).getText().length() > 0) {
   	                   // sign in the user ...
   	            	   setDDPIN(getBaseContext(),((EditText)v.findViewById(R.id.vs_login_PIN)).getText().toString());
   	            	   String username = ((EditText)alert11.findViewById(R.id.vs_login_username)).getText().toString();
   	            	   String password = ((EditText)alert11.findViewById(R.id.vs_login_password)).getText().toString();
   	            	   DoAppConfig(username,password,ServerUtilities.onlyTheNumbers(ServerUtilities.getSharedPhone(getBaseContext())));
   	            	 } else {
   	            		pinDLG.show(); 
   	            		displayMessage(getBaseContext(),"PIN cannot be blank.");
   	            	 }
   	               }
   	           })
   	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
   	               public void onClick(DialogInterface dialog, int id) {       	            	  
   	                  dialog.cancel();
   	                  pinDLG = null;   	                  
   	                  alert11.show();
   	               }
   	           });
   	   pinDLG = builder.create();
   	   pinDLG.setTitle("Create Outbound Calling PIN");
   	   pinDLG.show();      	
    }
    
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    	//Toast.makeText(getApplicationContext(), "Shared Value for "+key.toString(), Toast.LENGTH_LONG).show();
    	Log.i("in onSharedPreferenceChanged", key.toString());
    	try {
        	if (findPreference(key) instanceof CheckBoxPreference) { 
        		//sendReg(getBaseContext());
        		if (!IgnoreOnce) {
        		  	//GCMRegistrar.register(thisContext, SENDER_ID);
					Log.i(TAG, "onSharedPreferenceChanged: ");
					FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
						@Override
						public void onComplete(@NonNull Task<InstanceIdResult> task) {
							if (!task.isSuccessful()) {
								Log.w(TAG, "Preferences onSharedPreferenceChanged getInstanceId failed", task.getException());
								return;
							}

							Context context = getBaseContext();

							// Get new Instance ID token
							String token = task.getResult().getToken();
							Log.d(TAG, "vs Token " + token);

							String oldRegID = getRegID(context);
							setRegID(context,token);
							SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
							prefs.edit().putString(getString(R.string.pref_regid),token).commit();
							ServerUtilities.register(context, token,oldRegID);
						}
					});
        		} else {
        		  IgnoreOnce = false;
        		}
        		/*CheckBoxPreference cbPref = (CheckBoxPreference) findPreference(key);
        		if (cbPref.isChecked()) {
        			sendReg(getBaseContext());
        		}
        		else {
        			sendUnreg(getBaseContext());
        		}*/
        		updatePrefSummary(findPreference(key));
        	}    	
        	else {
               if (key.equalsIgnoreCase(getString(R.string.dd_pin_preferences)) && (sharedPreferences.getBoolean(getString(R.string.pref_send_alert), false))) {
            	  // sendReg(getBaseContext());	  
            	   updatePrefSummary(findPreference(key));
               } else {
            	   updatePrefSummary(findPreference(key));
            	   if (key.equalsIgnoreCase(getString(R.string.pref_local_phone))){ 
           			if (createloginDialog) {
           				createloginDialog = false;           				
           				createUsernamePasswordDialog();
           			}
            	  }
               }                       	 
    		}
        	
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}         
    }      
    
    public void DoAppConfig( String username, String password, String user_phone) {
    	pdialog = ProgressDialog.show(this, "Loading", "Please wait...", true);
    	mLoginTask = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
            	Map<String, String> param = new HashMap<String, String>();
            	param.put("VSUserName", URLEncoder.encode(params[0]));
            	param.put("VSPassword", URLEncoder.encode(params[1]));
            	param.put("user_phone", URLEncoder.encode(params[2]));
            	param.put("alogin", URLEncoder.encode("1"));
            	param.put("ddpin", URLEncoder.encode(getSharedPIN(getBaseContext())));            	
            	try {
					return ServerUtilities.post("https://secure.voiceshot.com/slogin/siteloginresp.asp",param);
				} catch (IOException e) {
					e.printStackTrace();
					return "";
				}
            }
            
            @Override
            protected void onPostExecute(String result) {
            	Log.i("onPostExecute ", result);
            	try {            		
					Map<String, List<String>> map  = getUrlParameters("?"+result);
					Log.i("onPostExecute", map.toString());
					
					if (map.get("error").get(0).equals("ok")) {
						clearAppData(getBaseContext());
						SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
				        SharedPreferences.Editor edit = prefs.edit();				        
				        edit.putBoolean("PREVIOUSLY_STARTED", Boolean.TRUE);				    
				        edit.putString(getString(R.string.pref_toast_position), "16");
				        //edit.putString(getString(R.string.pref_local_phone), ServerUtilities.onlyTheNumbers(ServerUtilities.getMyPhoneNumber(getBaseContext())));
				        edit.commit();												
					  	setDNIS(getBaseContext(),map.get("dnis").get(0));
					  	setDDPIN(getBaseContext(),map.get("ddpin").get(0));
					  	setNickName(getBaseContext(),map.get("nick").get(0));
					  	String [] vmboxesArr = map.get("mailboxes").get(0).split("\\|");
					  	for (String str : vmboxesArr) {
					  		if (str.contains(",")) {
					  			String[] separated = str.trim().split(",");
					  			setVMBoxStr(getBaseContext(),separated[0],separated[1]);
					  		}
					  	}
					  	IgnoreOnce = true;
					  	setRegID(getBaseContext(),"");

					  	setAllDnis(getBaseContext(),map.get("alldnis").get(0));
					  	setSendAndVMAlert(getBaseContext(),true);
					  	Handler handler = new Handler();
				        handler.postDelayed(new Runnable() {
				                public void run() {
				                	pdialog.dismiss();
								  	alert11.dismiss();
								  	alert11 = null;
								  	finish();
								  	displayMessage(getBaseContext(),"Configuration complete.");
				                }
				            }, 6000);



						//GCMRegistrar.register(thisContext, SENDER_ID);

					  	//startActivity(getIntent());
					  	
					} else if (map.get("error").get(0).equals("badlogin")) {
						pdialog.dismiss();
						displayMessage(getBaseContext(),"Incorrect Username/Password.");
						alert11.show();									  	
					} else if (map.get("error").get(0).equals("nophone")) {
						pdialog.dismiss();
						displayMessage(getBaseContext(),"Phone Number cannot be detected. Please configure manually.");
					} else if (map.get("error").get(0).equals("nodnis")) {
						pdialog.dismiss();
						displayMessage(getBaseContext(),"No VoiceShot Number in your account. Please sign up.");
					} else if (map.get("error").get(0).equals("nopin")) {
						pdialog.dismiss();
						//displayMessage(getBaseContext(),"Please enter a pin number.");
						createPINDialog();
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					pdialog.dismiss();
				}
            	mLoginTask = null;
            }            
    	};
    	mLoginTask.execute(username, password,user_phone);
    	
    }
    
    private void initSummary(Preference p){            
    	if (p instanceof PreferenceCategory){                 
    		PreferenceCategory pCat = (PreferenceCategory)p; 
    		for(int i=0;i<pCat.getPreferenceCount();i++){
    			initSummary(pCat.getPreference(i));                 }  
    		}else{ 
    			try {
					updatePrefSummary(p);
				} catch (NameNotFoundException e) {
					e.printStackTrace();
				}             } 
    }
    
    @Override //appears to not be liked in the lower java versions  
    public boolean onPreferenceChange(Preference preference, Object newValue) {
    	 	Log.i("OnPreferenceChange", newValue.toString());
    	// 	Toast.makeText(getApplicationContext(), "onPreferenceChange for "+preference.getKey().toString()+ " "+newValue.toString(), Toast.LENGTH_LONG).show();
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	try {
    		//Toast.makeText(getApplicationContext(), "Value for "+preference.getKey().toString()+" "+newValue.toString(), Toast.LENGTH_LONG).show();
    		if (preference.getKey().equalsIgnoreCase(getString(R.string.dd_pin_preferences))){ 	    			
    	      prefs.edit().putString(getString(R.string.dd_pin_preferences),SimpleCrypto.encrypt(getString(R.string.vs_enc_pw), ServerUtilities.onlyTheNumbers(newValue.toString()))).commit();
  			  if (prefs.getBoolean(getString(R.string.pref_send_alert), false)) {
				//sendReg(getBaseContext());	
  				//GCMRegistrar.register(thisContext, SENDER_ID);
			  }    	      
    		}
    		else if (preference.getKey().equalsIgnoreCase(getString(R.string.vm_pin_preferences))){ 
    		  prefs.edit().putString(getString(R.string.vm_pin_preferences),SimpleCrypto.encrypt(getString(R.string.vs_enc_pw), ServerUtilities.onlyTheNumbers(newValue.toString()))).commit();	
    		} else  if (preference.getKey().equalsIgnoreCase(getString(R.string.pref_local_phone))){ 
    			if (createloginDialog) {
    				createloginDialog = false;
    				createUsernamePasswordDialog();
    			}
    		}
        } catch (Exception e) { e.printStackTrace();  }
    	//Log.i("Preferences",prefs.getString(getString(R.string.dd_pin_preferences), ""));
    	try {
			updatePrefSummary(preference);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
    	return true;      
    }
    
    private void updatePrefSummary(Preference p) throws NameNotFoundException{
    	if (p instanceof ListPreference) {
    		ListPreference listPref = (ListPreference) p;
    		p.setSummary(listPref.getEntry());              
    	}
    	if (p instanceof EditTextPreference) {     		    		
    		EditTextPreference editTextPref = (EditTextPreference) p;     
    		if (p.getKey().equalsIgnoreCase(getString(R.string.dd_pin_preferences))){
    			String tmpStr = new String(new char[editTextPref.getText().length()]).replace('\0', '*'); 
    			p.setSummary(tmpStr);
    			
    		}
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.vs_phone_preferences))) {
    			if (editTextPref.getText().length() > 0) {
    			  p.setSummary(PhoneNumberUtils.formatNumber(editTextPref.getText()));
    			}
    			else {
    			  p.setSummary("Touch here to set your number");	     				
    			}
    		}
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.vm_box_preferences))) {
    			if (editTextPref.getText().length() > 0) {
    			  p.setSummary(PhoneNumberUtils.formatNumber(editTextPref.getText()));
    			}
    			else {
    			  p.setSummary("Touch here to set VM box number");	     				
    			}
    		}  
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.pref_nickname))) {
    			if (editTextPref.getText().length() > 0) {
    			  p.setSummary(editTextPref.getText());
    			}
    			else {
    			  p.setSummary("Enter a unique nicnkname for your phone");	     				
    			}
    		}    		
    		else {
    		 p.setSummary(editTextPref.getText());
    		}	
    	}  
    	if (p instanceof EncEditTextShowPINDialog) { 
    		//EncEditTextShowPINDialog editTextPref = (EncEditTextShowPINDialog) p;     
    		if (p.getKey().equalsIgnoreCase(getString(R.string.dd_pin_preferences))){ 
    		  String tmpStr = "";
    		  try {
    		    tmpStr = new String(new char[SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(getBaseContext())).getString(getString(R.string.dd_pin_preferences),"")).length()]).replace('\0', '*');
    		  } catch (Exception e) { e.printStackTrace();  }
    		  if (tmpStr.length() > 0){
    		    p.setSummary(tmpStr);
    		  }
    		  else { 
    			p.setSummary("Touch here to set your PIN");
    	      }    		  
    		}
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.vm_pin_preferences))){ 
      		  String tmpStr = "";
      		  try {
      		    tmpStr = new String(new char[SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(getBaseContext())).getString(getString(R.string.vm_pin_preferences),"")).length()]).replace('\0', '*');
      		  } catch (Exception e) { e.printStackTrace();  }
      		  if (tmpStr.length() > 0){
      		    p.setSummary(tmpStr);
      		  }
      		  else { 
      			p.setSummary("Touch here to set your VM PIN");
      	      } 
    		}
    	}

   	} 
 
	public void optionsDialClick(View view){         	        		
		finish();				
	}    
	
	public void optionscallLogClick(View view){         	        		
		//DialNumbers	mainActivity = getCallingActivity();
	   Intent resultIntent = new Intent();		
	   setResult(DialNumbers.RESULT_LOG_BUTTON_PUSHED, resultIntent);		
       finish();
	}
	
	public void optionscontactsClick(View view){        
		Intent resultIntent = new Intent();		
		setResult(DialNumbers.RESULT_CONTACT_BUTTON_PUSHED, resultIntent);        	        						
		finish();				
	} 		
	

	  private final BroadcastReceiver mHandleRefreshReceiver =
	          new BroadcastReceiver() {
	      @Override
	      public void onReceive(Context context, Intent intent) {
	  	      	
	    	  CheckBoxPreference mCheckBoxPreference;

	    	  mCheckBoxPreference = (CheckBoxPreference)getPreferenceScreen().findPreference(
	    			  getString(R.string.pref_send_alert));

	    	  if (mCheckBoxPreference != null) {
	    		  if (!mCheckBoxPreference.isChecked()){
	    	       mCheckBoxPreference.setChecked(false);
	    		  }
	    	  }
	          }
	          
	      };
	   
	      @Override
	      protected void onDestroy() {
	    	  unregisterReceiver(mHandleRefreshReceiver);
	          if (mRegisterTask != null) {
	              mRegisterTask.cancel(true);
	          }	    	  
	          super.onDestroy();	      	    	 
	      }
	      
	      class Validator implements AutoCompleteTextView.Validator {

	          @Override
	          public boolean isValid(CharSequence text) {
	              Log.v("Test", "Checking if valid: "+ text);            
	              if (text.length()==10) {
	                  return true;
	              }

	              return false;
	          }

	          @Override
	          public CharSequence fixText(CharSequence invalidText) {
	              Log.v("Test", "Returning fixed text");

	              /* I'm just returning an empty string here, so the field will be blanked,
	               * but you could put any kind of action here, like popping up a dialog?
	               * 
	               * Whatever value you return here must be in the list of valid words.
	               */
	              //"Number must be 10 digits long."
	              return "";
	          }
	      }

	      class FocusListener implements View.OnFocusChangeListener {

	          @Override
	          public void onFocusChange(View v, boolean hasFocus) {
	              Log.v("Test", "Focus changed");
	              if ((v instanceof AutoCompleteTextView) && !hasFocus) {
	                  Log.v("Test", "Performing validation");
	                  ((AutoCompleteTextView)v).performValidation();
	              }
	          }
	      }  	      
    
}    
