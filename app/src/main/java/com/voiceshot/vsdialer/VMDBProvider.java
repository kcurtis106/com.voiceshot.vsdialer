package com.voiceshot.vsdialer;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class VMDBProvider {

    public static final String KEY_TIMESTAMP = "dateandtime";
    public static final String KEY_PHONE = "phonenumber";
    public static final String KEY_DURATION = "duration";
    public static final String KEY_BOX = "box";
    public static final String KEY_FILENAME = "filename";
    public static final String KEY_ROWID = "_id";
    public static final String KEY_ISNEW = "isnew";

    private static final String TAG = "VMDbAdapter";
    private MyDatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    

    private static final String DATABASE_CREATE =
            "create table voicemail (_id integer primary key autoincrement, "
                    + "dateandtime text not null, phonenumber text not null, duration text not null, box text not null, filename text not null,isnew integer not null);";

    private static final String DATABASE_NAME = "VMdata";
    private static final String DATABASE_TABLE = "voicemail";
    private static final String VM_INITIAL_QUERY = "select Cast(box as integer) as _id,sum(isnew) as NewMail,sum(case when isnew=0 then 1 else 0 end) as SavedMail from voicemail group by Cast(box as integer) order by Cast(box as integer)";
    private static final String VM_INITIAL_QUERY_BOXES = "select box as [_id], ifnull(newmail,0) as [NewMail], ifnull(SavedMail,0) as [SavedMail]  from ("; 
    private static final String VM_INITIAL_QUERY_BOXES_END	= ") as t2 left join (select Cast(box as integer) as _id,sum(isnew) as NewMail,sum(case when isnew=0 then 1 else 0 end) as SavedMail from voicemail group by Cast(box as integer)) as t1 on t1._id = t2.box order by _id";
    private static final int DATABASE_VERSION = 3;

    private final Context mCtx;

    private static class MyDatabaseHelper extends SQLiteOpenHelper {

    	MyDatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(DATABASE_CREATE);         
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS vm");
            db.execSQL("DROP TABLE IF EXISTS voicemail");
            onCreate(db);
        }
    }


    public VMDBProvider(Context ctx) {
        this.mCtx = ctx;
    }


    public VMDBProvider open() throws SQLException {
        mDbHelper = new MyDatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }
    
    public void close() {
        mDbHelper.close();
    }



    public long createVMEntry(String dateandtime, String phonenumber,String duration,String box,String filename,int isnew) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TIMESTAMP, dateandtime);
        initialValues.put(KEY_PHONE, phonenumber);
        initialValues.put(KEY_DURATION, duration);
        initialValues.put(KEY_BOX, box);
        initialValues.put(KEY_FILENAME, filename);
        initialValues.put(KEY_ISNEW, isnew);
        return mDb.insert(DATABASE_TABLE, null, initialValues);
    }

      public boolean deleteVMEntry(long rowId) {

        return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }
      
    public boolean deleteBoxEntry(String boxnum) {
          return mDb.delete(DATABASE_TABLE, KEY_BOX + "=" + boxnum, null) > 0;
    }      

    public boolean deleteALLVMEntry() {

        return mDb.delete(DATABASE_TABLE,null, null) > 0;
    }    

    public Cursor fetchAllVM() {

        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TIMESTAMP,
        		KEY_PHONE,KEY_DURATION,KEY_BOX,KEY_FILENAME}, null, null, null, null, "_ID DESC");
    }
    
    public Cursor fetchAllBoxVM(String curBox, int isNew) {

        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TIMESTAMP,
        		KEY_PHONE,KEY_DURATION,KEY_BOX,KEY_FILENAME}, "box = ? and isnew = ?", new String[] {curBox,Integer.toString(isNew)}, null, null, "_ID DESC");
    }    

    public Cursor fetchVM(long rowId) throws SQLException {

        Cursor mCursor =

                mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TIMESTAMP,
                		KEY_PHONE,KEY_DURATION,KEY_BOX,KEY_FILENAME}, KEY_ROWID + "=" + rowId, null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    
    public Cursor fetchMailboxes(ArrayList<String> boxList) {
    	if (boxList.size()==0) {
    	 return mDb.rawQuery(VM_INITIAL_QUERY, null);
    	} else {
    	  String tmpRow = "";
    	  for (int i = 0; i < boxList.size(); i++) {
    		  if (tmpRow=="") {
    			  tmpRow = "select "+boxList.get(i)+ " as [box]";
    		  } else {
    			  tmpRow += " union all select "+boxList.get(i);
    		  }    		  
    	  }
    	  return mDb.rawQuery(VM_INITIAL_QUERY_BOXES+tmpRow+VM_INITIAL_QUERY_BOXES_END, null);
    	}
    }

    public boolean saveVMEntry(long rowId) {
    	ContentValues args = new ContentValues();
    	args.put("isnew","0");
    	return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

 /*   public boolean updateNote(long rowId, String VMthing1) {
        ContentValues args = new ContentValues();
       // args.put(KEY_, );       

        return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }*/
}
