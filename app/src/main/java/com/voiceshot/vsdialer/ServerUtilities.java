package com.voiceshot.vsdialer;


import static com.voiceshot.vsdialer.CommonUtilities.SERVER_URL;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.CommonUtilities.sendRemoveBox;
import static com.voiceshot.vsdialer.CommonUtilities.displayMessage;
import static com.voiceshot.vsdialer.CommonUtilities.refreshPref;

import com.voiceshot.vsdialer.VMDBProvider;

import android.content.Context;
import android.content.pm.PackageManager;
import android.Manifest;
import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.MainThread;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import android.support.v4.content.ContextCompat;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

/**
 * Helper class used to communicate with the demo server.
 */
public final class ServerUtilities {

    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();
    public static boolean fromVMChild = false;
    public static String notifyVMbox = "";
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;

    static void clearAppData(Context context){
    	String tmpString = getSharedPhone(context);
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().clear().commit();
    	setSharedPhone(context,tmpString);
       /* VMDBProvider mDbHelper = new VMDBProvider(context);
        mDbHelper.open();
        mDbHelper.deleteALLVMEntry();
        mDbHelper.close();*/
    }
    

    static String getMyPhoneNumber(Context context, Activity activity)
    {
    	String aStr = "";
        boolean getPhoneNumber = false;
        //commented on 2/27/19, instead of asking for permission to get phone number programatically (which may be denied), just ask user to enter phone number

    	/*if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

        }
        else
            getPhoneNumber = true;

        if (getPhoneNumber) {
            if (((TelephonyManager) (context.getSystemService(Context.TELEPHONY_SERVICE))).getLine1Number() == null) {
                return aStr;
            } else
                return ((TelephonyManager) (context.getSystemService(Context.TELEPHONY_SERVICE))).getLine1Number();
        }
        else */
            return aStr;
    }  
    
    static void setSharedPhone(Context context, String DNIS) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_local_phone),DNIS).commit();
    }  

    
    static String getSharedPhone(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = onlyTheNumbers(pref.getString(context.getString(R.string.pref_local_phone),""));
    	return aStr;
    }     
    
    static String getSharedPIN(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = SimpleCrypto.decrypt(context.getString(R.string.vs_enc_pw),pref.getString(context.getString(R.string.dd_pin_preferences),""));
    	return aStr;
    }  
    
    static String getDNIS(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = onlyTheNumbers(pref.getString(context.getString(R.string.vs_phone_preferences),""));
    	return aStr;
    }      
    
    static String getNickName(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = pref.getString(context.getString(R.string.pref_nickname),"");
    	Log.i(TAG, "getNickName = " + aStr );
    	return aStr;
    }     
         
    static String getRegID(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = pref.getString(context.getString(R.string.pref_regid),"");    	
    	return aStr;
    }     
    
    static String getAllDNIS(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = pref.getString(context.getString(R.string.pref_dnis_arr),"");    	
    	return aStr;
    }     
        
    static void setRegID(Context context, String RegistrationID) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_regid),RegistrationID).commit();
    }
        
    public static String onlyTheNumbers(
            final CharSequence input ){
    final StringBuilder sb = new StringBuilder(
            input.length() );
    for(int i = 0; i < input.length(); i++){
        final char c = input.charAt(i);
        if(c > 47 && c < 58){
            sb.append(c);
        }
    }
    return sb.toString();
}    
    
    static String getSendAlert(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	if (pref.getBoolean(context.getString(R.string.pref_send_alert),false)) {
    		return "1";	
    	}
    	else {
    		return "0";	
    	}    	
    }    
    
    static String getSendVMAlert(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	if (pref.getBoolean(context.getString(R.string.pref_vm_notify_alert),false)) {
    		return "1";	
    	}
    	else {
    		return "0";	
    	}    	
    }  
    
    static void setSendAndVMAlert(Context context, boolean curSendAlert) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	SharedPreferences.Editor edit = pref.edit();
    	edit.putBoolean(context.getString(R.string.pref_vm_notify_alert),curSendAlert);
    	edit.putBoolean(context.getString(R.string.pref_send_alert),curSendAlert);
    	edit.commit();
    }    
        
    
    static void setSendAlert(Context context, boolean curSendAlert) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putBoolean(context.getString(R.string.pref_send_alert),curSendAlert).commit();
    }    
    
    static void setSendVMAlert(Context context, boolean curVMAlert) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putBoolean(context.getString(R.string.pref_vm_notify_alert),curVMAlert).commit();
    }    
    
    static void setNickName(Context context, String NickName) {
    	Log.i(TAG, "setNickName = " + NickName );
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_nickname),NickName).commit();
    }  
    
    static void setAllDnis(Context context, String DNIS) {
    	Log.i(TAG, "setNickName = " + DNIS );
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_dnis_arr),DNIS).commit();
    }     
    
    static void setDNIS(Context context, String DNIS) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.vs_phone_preferences),DNIS).commit();
    }     
    
    static void setDDPIN(Context context, String DDPIN) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.dd_pin_preferences),SimpleCrypto.encrypt(context.getString(R.string.vs_enc_pw),DDPIN)).commit();
    }  
    
    static public void setVMBoxStr (Context context,String vmbox, String vmpin) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
    	int max = pref.getInt("numboxes",0);
    	pref.edit().putString("box"+max,vmbox).commit();
    	pref.edit().putString("pin"+max,SimpleCrypto.encrypt(context.getString(R.string.vs_enc_pw),vmpin)).commit();
    	max++;
    	pref.edit().putInt("numboxes",max).commit();
    }
    
    public static String getVMBoxStr (Context context) {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    	int max = prefs.getInt("numboxes",0);
    	String URLStr = "";
    	for (int i = 0; i < max; i++) {
    		if (URLStr.length()>0) {URLStr += "||";}
    		URLStr += prefs.getString("box"+i,"")+",";
    		try {
    			URLStr += SimpleCrypto.decrypt(context.getResources().getString(R.string.vs_enc_pw),prefs.getString("pin"+i,""));
			} catch (Exception e) {				 
				e.printStackTrace();				
			}
    		
    	}
    	return URLStr;
    } 
    
    public static String delVMBox (Context context,String vmbox) {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    	int max = prefs.getInt("numboxes",0);
    	ArrayList<String> boxArray = new ArrayList<String>();
    	ArrayList<String> pinArray = new ArrayList<String>();
    	for (int i = 0; i < max; i++) {
    		if (!vmbox.contentEquals(prefs.getString("box"+i,"")) ) {    		  
    			boxArray.add(prefs.getString("box"+i,""));    			
    		    try {
    		    	pinArray.add(SimpleCrypto.decrypt(context.getResources().getString(R.string.vs_enc_pw),prefs.getString("pin"+i,"")));
			    } catch (Exception e) {				 
				   e.printStackTrace();				
			   }
    	   }	    	
    	}    	
    	String tmpStr = "";
    	prefs.edit().putInt("numboxes",boxArray.size()).commit();
    	for (int i = 0; i < boxArray.size(); i++) {
    		tmpStr += boxArray.get(i) + "|";
    		prefs.edit().putString("box"+(i),boxArray.get(i)).commit();
    		prefs.edit().putString("pin"+(i),SimpleCrypto.encrypt(context.getString(R.string.vs_enc_pw),pinArray.get(i))).commit();    		    		
    	}
    	return tmpStr;
    }     
    
    static void setCurVM(Context context, String curVM) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_curVM),curVM).commit();
    }  

    
    static String getCurVM(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = pref.getString(context.getString(R.string.pref_curVM),"");
    	return aStr;
    }     
    
    static void setOldVM(Context context, String oldVM) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_oldVM),oldVM).commit();
    }  

    
    static String getOldVM(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = pref.getString(context.getString(R.string.pref_oldVM),"");
    	return aStr;
    }    
    
    static void setOldPIN(Context context, String oldPIN) {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	pref.edit().putString(context.getString(R.string.pref_oldPIN),oldPIN).commit();
    }  

    
    static String getOldPIN(Context context)
    {
    	SharedPreferences pref = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE); 
    	String aStr = pref.getString(context.getString(R.string.pref_oldPIN),"");
    	return aStr;
    }      
    
    /**
     * Register this account/device pair within the server.
     *
     * @return whether the registration succeeded or not.
     */
    static boolean register(final Context context, final String regId, final String oldregId) {
      //  Log.i(TAG, "registering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL + "/registerv3.asp";
        int versionCode = BuildConfig.VERSION_CODE;
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", URLEncoder.encode(regId));
        params.put("oldregId", URLEncoder.encode(oldregId));
        String sendVMAlert = URLEncoder.encode(getSendVMAlert(context));
        params.put("VMAlert", sendVMAlert);  
        String sendAlert = URLEncoder.encode(getSendAlert(context));
        params.put("Alert", sendAlert);    
      //  if (sendVMAlert.equals("1") || sendAlert.equals("1")) {
          if (sendVMAlert.equals("1")) { 
              String VMBoxes = URLEncoder.encode(getVMBoxStr(context).replaceAll("\\s+",""));
              if (VMBoxes.isEmpty()) {
            	  sendVMAlert = "0";  
            	  if (sendAlert.equals("0")) {
            		// abort, nothing to send.
            		//return true;  
            	  }
              }
              else {
                params.put("VMBoxes",VMBoxes);
              }   
              
          }        	
          String userPhonenum = URLEncoder.encode(getSharedPhone(context).replaceAll("\\s+",""));
          params.put("phonenumber",userPhonenum);
          String userPIN = URLEncoder.encode(getSharedPIN(context).replaceAll("\\s+",""));
          params.put("PIN",userPIN);       
          String DNIS = URLEncoder.encode(getDNIS(context).replaceAll("\\s+",""));
          params.put("DNIS",DNIS); 
          String NickName = URLEncoder.encode(getNickName(context));
          params.put("NickName", NickName);
          params.put("appver", Integer.toString(versionCode));
          if (userPhonenum.isEmpty()) {
        	 displayMessage(context, "\"Your cell phone number\" must be provided in the preference screen.");  
        	return false;  
          }
          if (userPIN.isEmpty()) {
        	displayMessage(context, "Your Outbound calling PIN must be provided in the preference screen.");
        	return false;  
          }
          if (DNIS.isEmpty()) {
        	displayMessage(context, "Your VoiceShot Number must be provided in the preference screen.");  
        	return false;  
          }
          if (NickName.isEmpty()) {
        	displayMessage(context, "Your Nickname must be provided in the preference screen.");  
        	return false;  
          }          
          
    //    }
    //    else {
    //    	serverUrl = SERVER_URL + "/unregisterv2.asp";
    //    }      
        
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        
        // Once GCM returns a registration id, we need to register it in the
        // demo server. As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to send registration info");
            try {
              //  displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
            	String postResult = post(serverUrl, params);
            	//displayMessage(context,"Post Result: "+postResult);
                if (postResult.contentEquals("ok")) {
                 //  GCMRegistrar.setRegisteredOnServer(context, true);
                  // setRegID(context,regId);
                	
                   return true;
                }
                else {
                	//unregister(context,regId);                	       	
                	if (postResult.contentEquals("1")) {
                	  refreshPref(context); 	
                	  displayMessage(context, "Invalid Outbound Calling PIN.  Tap Configure App at top of the screen.");
                	}
                	else if (postResult.contentEquals("3")) {
                		displayMessage(context, "This phone is not authorized for use with your Voiceshot account. To authorize, tap Configure App at the top of the screen.");
                	}
                	else {
                		sendRemoveBox(context);
                		displayMessage(context, "This box number and pin combination does not match any in your account.");
                		

                	}
                	return false;
                }

                
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        String message = context.getString(R.string.server_register_error,       MAX_ATTEMPTS);
        return false;
    }

    /**
     * Unregister this account/device pair within the server.
     */
    static void unregister(final Context context, final String regId) {
     //   Log.i(TAG, "unregistering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL + "/unregisterv2.asp";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(serverUrl, params);
            String message = context.getString(R.string.server_unregistered);

        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            String message = context.getString(R.string.server_unregister_error,
                    e.getMessage());
            CommonUtilities.displayMessage(context, message);
        }
    }
    

    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    public static String post(String endpoint, Map<String, String> params)
            throws IOException {
        URL url;
        String iResp = "-1";
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();        
        Log.d(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
           // String resp = conn.getResponseMessage();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));                 
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                total.append(line);
            }            
            /* int status = conn.getResponseCode();
            
            if (status != 200) {
              
            }*/
           /* if (resp.equalsIgnoreCase("ok")) {
            	iResp = 1;
            }*/
            iResp = total.toString();
            if (iResp == null) {iResp = "-1";}

            if (conn != null) {
                conn.disconnect();
            }
         return iResp;          
      }
  
  
    
    public static Map<String, List<String>> getUrlParameters(String url)
            throws UnsupportedEncodingException {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        String[] urlParts = url.split("\\?");
        if (urlParts.length > 1) {
            String query = urlParts[1];
            for (String param : query.split("&")) {
                String pair[] = param.split("=");
                String key = URLDecoder.decode(pair[0], "UTF-8");
                String value = "";
                if (pair.length > 1) {
                    value = URLDecoder.decode(pair[1], "UTF-8");
                }
                List<String> values = params.get(key);
                if (values == null) {
                    values = new ArrayList<String>();
                    params.put(key, values);
                }
                values.add(value);
            }
        }
        return params;
    }    
}
