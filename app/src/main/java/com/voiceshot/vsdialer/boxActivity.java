package com.voiceshot.vsdialer;

import static com.voiceshot.vsdialer.CommonUtilities.SERVER_URL;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.ServerUtilities.getNickName;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPhone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupMenu;

class MyVMCursorAdapter extends SimpleCursorAdapter   {
	private Context appContext;	
	private ListView notifyList; 
	
    public MyVMCursorAdapter(Context context, int layout, Cursor c, String[] from,int[] to, ListView inList) {
        super(context, layout, c, from, to);
        this.appContext=context;
        this.notifyList=inList;
        
    }
    
   
    @Override
    public View getView(final int position, View convertView,final ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);    	
        Cursor cursor = (Cursor) getItem(position);       
        ContactResult cr = CommonUtilities.getContactName(appContext,cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
        ImageView b = (ImageView) view.findViewById(R.id.list_image);
       // ((Activity)appContext).registerForContextMenu(b); 
        b.setOnTouchListener(null);
      /*  b.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {             	  
            	((Activity)appContext).openContextMenu( v );
                  return true;
            }
        });*/
        TextView phTV = (TextView) view.findViewById(R.id.notify_phone_number);
     //   ((Activity)appContext).registerForContextMenu(phTV); 
       /* phTV.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {                  
                  return false;
            }
        });   */     
        TextView midTV = (TextView) view.findViewById(R.id.notify_extra_info);
      //  ((Activity)appContext).registerForContextMenu(midTV); 
       /* midTV.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {                  
                  return false;
            }
        });  */      
        ImageView phoneIcon = (ImageView) view.findViewById(R.id.imageView1);
      //  ((Activity)appContext).registerForContextMenu(phoneIcon);
        /*phoneIcon.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {                  
                  return false;
            }
        });*/        
        phoneIcon.setImageResource(R.drawable.ic_call_rotate);
        if (cr.getContactName() != null){        	
            if (cr.getContactType() != null){
            	phTV.setText( cr.getContactName());
            	midTV.setVisibility(View.VISIBLE); 
            	midTV.setText(cr.getContactType() + " " + cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
            }
            else {
            	phTV.setText(cr.getContactName());	
            	midTV.setVisibility(View.GONE);
            	midTV.setText("");
            }
        }
        else {
        	if (cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)).length() == 0) {
        		phTV.setText("Private Caller");      
        		phoneIcon.setImageResource(R.drawable.ic_call_rotate_grey);
        	}
        	else {        		        	
        	  phTV.setText(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
        	}
        	midTV.setVisibility(View.GONE);
        	midTV.setText("");
        }
        
        TextView dtTV = (TextView) view.findViewById(R.id.notify_date_time);
      //  ((Activity)appContext).registerForContextMenu(dtTV);
        dtTV.setText(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_TIMESTAMP)));
       /* dtTV.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {                  
                  return false;
            }
        });  */       
        
        TextView durTV = (TextView) view.findViewById(R.id.notify_duration);
      //  ((Activity)appContext).registerForContextMenu(durTV);
        durTV.setText(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_DURATION)));
       /* durTV.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {                  
                  return false;
            }
        });*/
        
        if (cr.getBitmap() != null) {
        	b.setImageBitmap(cr.getBitmap());
        } else {
        	b.setImageResource(R.drawable.ic_play_circle2);
        }

        return view;
    }       
}



public class boxActivity extends Activity implements OnPreparedListener, MediaController.MediaPlayerControl {
	private VMDBProvider mDbHelper;
	private Cursor vm_cursor;
	private ListView notifyList; 
	private String curBox = "";
	private int isNew = 0;
	private SimpleCursorAdapter VMList;
	MediaPlayer mediaPlayer  = null;
	private VMMediaController mediaController = null;
	public String currentFileName = "";
	public String dlFileName = "";
	private Handler handler = new Handler();
	static final int DELETE_VM_MENU = 2;
	static final int MOVE_NEW_VM_MENU = 3;
	DownloadTask downloadTask = null;
	ProgressDialog mProgressDialog; 
	
	public class VMMediaController extends MediaController {

	    public VMMediaController(Context context, boolean useFastForward) {
	        super(context, useFastForward);

	    }

	    @Override
	    public void hide() {
	        //mediaController.show(0);
	    }
	    
	    public void superhide() {
	    	super.hide();
	    }       
	    

	}  	
	
	public void onCreate(Bundle savedInstanceState) { 
		RelativeLayout rl;
		TextView t;
        super.onCreate(savedInstanceState);
        mDbHelper = new VMDBProvider(this); 
        mDbHelper.open();         
		setContentView(R.layout.box_list_view);		
		notifyList = (ListView)findViewById(R.id.vm_box_full_list);
		curBox = getIntent().getStringExtra("box");		
		isNew = getIntent().getIntExtra("isNew",0); 
		rl = (RelativeLayout)findViewById(R.id.box_header_bar);
		t  = (TextView)findViewById(R.id.vm_box_title); 
		if (isNew == 0) {
		  rl.setBackgroundColor(Color.parseColor("#C87137"));
		  t.setText("Box " +curBox+ " Saved Voicemail");		  		  
		} else {
		  rl.setBackgroundColor(Color.parseColor("#008000"));	
		  t.setText("Box " +curBox+ " New Voicemail");
		}
		fillBoxData();
		registerForContextMenu(notifyList);
		
	    mProgressDialog = new ProgressDialog(getParent());	    
	    mProgressDialog.setMessage("Loading Voicemail");
	    mProgressDialog.setIndeterminate(true);
	    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	    mProgressDialog.setCancelable(true);			
	}
	
    private void fillBoxData() {	    		    		    	
    	vm_cursor = mDbHelper.fetchAllBoxVM(curBox,isNew);
        startManagingCursor(vm_cursor);
        
        // Create an array to specify the fields we want to display in the list (only TITLE)
        String[] from = new String[]{VMDBProvider.KEY_PHONE,VMDBProvider.KEY_TIMESTAMP};
        
        // and an array of the fields we want to bind those fields to (in this case just text1)
        int[] to = new int[]{R.id.notify_phone_number,R.id.notify_date_time};
        
        VMList =  new MyVMCursorAdapter(this, R.layout.vm_notify_row, vm_cursor, from, to,notifyList); 
     //   notifyList = (ListView)findViewById(R.id.vm_box_full_list);
        notifyList.setAdapter(VMList);	     
        notifyList.setOnItemClickListener(new OnItemClickListener() {
        	   public void onItemClick(AdapterView<?> parent, View v,int position, long id) {
        		   listenToVMClick(position,getViewByPosition(position,notifyList));
        	   } });
        	
    }	
    
    
    public void back_btn(View v) {
    	com.voiceshot.vsdialer.ServerUtilities.fromVMChild = true;
    	finish();
    }
    
    public void phoneClick(View v) {	    	
   
    	final int phonePos = notifyList.getPositionForView(v);
    	final PopupMenu popup = new PopupMenu(this, v);
        popup.getMenuInflater().inflate(R.menu.vm_popup_phone,
                popup.getMenu());
        popup.show();    	
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean whichRes = false;
                switch (item.getItemId()) {
                case R.id.group_item_one:
                	Cursor cursor = (Cursor) VMList.getItem(phonePos);  
                	if (cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)).length()!=0) {
                		Uri number = Uri.parse("tel:"+cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
                		Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                		startActivity(callIntent);		
                	} 
                    whichRes =  true;
                    break;
                case R.id.group_item_two:
                	Cursor cursor2 = (Cursor) VMList.getItem(phonePos);   	        
                	if (cursor2.getString(cursor2.getColumnIndex(VMDBProvider.KEY_PHONE)).length()!=0) {
                		((TabGroupActivity)getParent()).setDial(cursor2.getString(cursor2.getColumnIndex(VMDBProvider.KEY_PHONE)));    		
                	} 
                    whichRes = true;
                    break;

                default:                	
                	break;
                }
                if (!whichRes ){                	
                    item.setShowAsAction(8);
                    item.setActionView(new View(getBaseContext()));               	
                	
                }
                return whichRes;
                
            }
        });        
    /*	Cursor cursor = (Cursor) VMList.getItem(position);   	        
    	if (cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)).length()!=0) {
    		((TabGroupActivity)getParent()).setDial(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));    		
    	} */
     }	    
    
    
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
      if (v.getId()==R.id.vm_box_full_list) {	
    	  if (isNew == 1) {
    	    menu.add(Menu.NONE,MOVE_NEW_VM_MENU,MOVE_NEW_VM_MENU,"Move to saved");
    	  } 
	      menu.add(Menu.NONE, DELETE_VM_MENU, DELETE_VM_MENU, "Delete voicemail");	 
	  } 
	} 
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
      if (item.getItemId() == DELETE_VM_MENU) {		  
		  if (isOnline()) {			 
		    Cursor cursor = (Cursor) VMList.getItem(info.position);	
		    String curfilename = cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_FILENAME));		    
		    if (fileExists(curfilename+".wav")) {			    
			     File tmpFile = new File(getFilesDir()+curfilename+".wav");  
			     tmpFile.delete();						   
		    }
		    mDbHelper.deleteVMEntry(cursor.getLong(0));
		    fillBoxData();
		    DeleteVMTask deleteTask = new DeleteVMTask(boxActivity.this,curfilename+".wav",this);
		    deleteTask.execute(SERVER_URL+"/vmdel.asp?fn="+URLEncoder.encode(curfilename)+"&nick="+URLEncoder.encode(getNickName(getApplicationContext()).replaceAll("\\s+",""))+"&ph="+URLEncoder.encode(getSharedPhone(getApplicationContext()).replaceAll("\\s+",""))+"&regid="+URLEncoder.encode(getRegID(getApplicationContext())));			  
		  }
		  else {
			  Toast.makeText(getApplicationContext(), "Unable to delete voicemail message. Make sure data is enabled on your phone and your app settings are correct.", Toast.LENGTH_LONG).show();  
		  }
	  } else if (item.getItemId() == MOVE_NEW_VM_MENU) {	
		  Cursor cursor = (Cursor) VMList.getItem(info.position);
		  String curfilename = cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_FILENAME));
		  mDbHelper.saveVMEntry(cursor.getLong(0));
		  SaveVMTask saveTask = new SaveVMTask(boxActivity.this,curfilename+".wav",this);
		  saveTask.execute(SERVER_URL+"/vmsave.asp?fn="+URLEncoder.encode(curfilename)+"&nick="+URLEncoder.encode(getNickName(getApplicationContext()).replaceAll("\\s+",""))+"&ph="+URLEncoder.encode(getSharedPhone(getApplicationContext()).replaceAll("\\s+",""))+"&regid="+URLEncoder.encode(getRegID(getApplicationContext())));		  
		  fillBoxData();
	  }
	  return true;
	}	
    
    
    //--MediaPlayerControl methods----------------------------------------------------
    public void start() {
    	//Log.d("com.voiceshot.vsdialer", "vsdialer start"); 	
      mediaPlayer.start();
    }

    public void pause() {
    //	Log.d("com.voiceshot.vsdialer", "vsdialer pause"); 	
      mediaPlayer.pause();
    }

    public int getDuration() {
    //	Log.d("com.voiceshot.vsdialer", "vsdialer getDuration"); 	
      return mediaPlayer.getDuration();
    }

    public int getCurrentPosition() {
    //	Log.d("com.voiceshot.vsdialer", "vsdialer getCurrentPosition"); 	
      return mediaPlayer.getCurrentPosition();
    }

    public void seekTo(int i) {
    	//Log.d("com.voiceshot.vsdialer", "vsdialer seekTo"); 	
      mediaPlayer.seekTo(i); 
    }

    public boolean isPlaying() {
    	//Log.d("com.voiceshot.vsdialer", "vsdialer isPlaying"); 
      return mediaPlayer.isPlaying();
    }

    public int getBufferPercentage() {
    	//Log.d("com.voiceshot.vsdialer", "vsdialer getBufferPercentage"); 
      return 0;
    }

    public boolean canPause() {
      return true;
    }

    public boolean canSeekBackward() {
      return true; 
    }

    public boolean canSeekForward() {
      return true;
    }
    //--------------------------------------------------------------------------------

    @Override
    protected void onStop() {
      super.onStop();    
      //Log.d("com.voiceshot.vsdialer", "onStop");
      if (mediaController != null) {
    	mediaController.superhide();  
        mediaController = null;  
      }
      if (mediaPlayer != null) {
  	    mediaPlayer.reset();
  	    mediaPlayer.release();
  	    mediaPlayer = null;
      }
      
    }


    public void onPrepared(MediaPlayer mediaPlayer) {
        //Log.d("com.voiceshot.vsdialer", "onPrepared");
        handler.post(new Runnable() {
          public void run() {
            mediaController.setEnabled(true);
            mediaController.show();
           
          }
        });
        
      }

   // @Override
    public int getAudioSessionId() {
    	// TODO Auto-generated method stub
    	//Log.d("com.voiceshot.vsdialer", "getAudioSessionId");
    	return 0;
    }	    

    public boolean fileExists(String filename) {
    	File file = new File(getFilesDir()+filename);
    	if(file.exists()) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    @Override
    public void onPause() {
    	if (downloadTask != null) {
    		downloadTask.cancel(true);
    	}
	    if (mediaController != null) {
		  	mediaController.superhide();  
		    mediaController = null;  
		}
	    if (mediaPlayer != null) {
	        mediaPlayer.reset();
	        mediaPlayer.release();
	        mediaPlayer = null;
	    }	    	
	    
	    if (fileExists(currentFileName)) {			   
		  File tmpFile = new File(getFilesDir()+currentFileName); 
		  tmpFile.delete();		
		  currentFileName = "";
		}	
	    mProgressDialog.dismiss();      
		if (fileExists(dlFileName)) {			    
			     File tmpFile = new File(getFilesDir()+dlFileName);  
			     tmpFile.delete();						   
		}	    
	    if (!this.isFinishing() ) finish();
    	super.onPause();
    }		
    

    public boolean isOnline() {
        ConnectivityManager cm =
            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && 
           cm.getActiveNetworkInfo().isConnectedOrConnecting();
    } 
    
    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }    
    
	public void listenToSpeakerVMClick(View v) {		
		//listenToVMClick((View)v.getParent().getParent());notifyList.getPositionForView(v);
		int position = notifyList.getPositionForView(v);
		listenToVMClick(position,getViewByPosition(position,notifyList));
	}    
    
	public void listenToVMClick(int position,View v) {		
    	//int position = notifyList.getPositionForView(v);	 
		if (mediaController == null) {
    	Cursor cursor = (Cursor) VMList.getItem(position); 
    	final String curfilename = cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_FILENAME));    	//
	    String AUDIO_FILE_NAME = getFilesDir()+curfilename+".wav";
	    if (fileExists(curfilename+".wav")) {
          File tmpFile2 = new File(getFilesDir()+curfilename+".wav");
          //Log.d("com.voiceshot.vsdialer", "vsdialer file size "+tmpFile2.length()); 
          if (tmpFile2.length()==0){
            tmpFile2.delete();
            tmpFile2 = null;
          }
	    }
	    if (!fileExists(curfilename+".wav")) {
		      if (mediaController != null) {
		    	    mediaController.superhide();  
		        	mediaController = null;  
		          }
	          if (mediaPlayer != null) {
	        	  mediaPlayer.reset();
	        	  mediaPlayer.release();
	        	  mediaPlayer = null;
	          }	    	
	       dlFileName   = curfilename+".wav";
       	   downloadTask = new DownloadTask(boxActivity.this,v,curfilename+".wav",this);
	       downloadTask.execute(SERVER_URL+"/vmdl.asp?fn="+URLEncoder.encode(curfilename)+"&nick="+URLEncoder.encode(getNickName(getApplicationContext()))+"&ph="+URLEncoder.encode(getSharedPhone(getApplicationContext()).replaceAll("\\s+",""))+"&regid="+URLEncoder.encode(getRegID(getApplicationContext())));

	        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
	          @Override
	          public void onCancel(DialogInterface dialog) {
	              if (downloadTask != null) { downloadTask.cancel(true); }
	              //Log.d("com.voiceshot.vsdialer", "vsdialer onCancel"); 
	     	      if (fileExists(currentFileName+".wav")) {		
	     	    	//Log.d("com.voiceshot.vsdialer", "vsdialer deleting");  
	 	   		    File tmpFile = new File(getFilesDir()+currentFileName+".wav");
	 	   		    if (!tmpFile.delete()) {
	 	   		       //Log.d("com.voiceshot.vsdialer", "vsdialer not deleted "+currentFileName); 
	 	   		    }
	 	   		    tmpFile = null;
	 	   		  }
			      if (mediaController != null) {
			    	    mediaController.superhide();  
			        	mediaController = null;  
			          }
		          if (mediaPlayer != null) {
		        	  mediaPlayer.reset();
		        	  mediaPlayer.release();
		        	  mediaPlayer = null;
		          }	   	     	      
	     	      if (fileExists(curfilename+".wav")) {     	    	
	   	   		    File tmpFile = new File(getFilesDir()+curfilename+".wav");
	   	   		    if (!tmpFile.delete() )  {
	   	   		     //Log.d("com.voiceshot.vsdialer", "vsdialer not deleted "+curfilename); 
	   	   		    }
	   	   		    
	   	   		    tmpFile = null;
	     	      }
	              Toast.makeText(getApplicationContext(),"Download cancelled.", Toast.LENGTH_SHORT).show(); 
	          }
	      });
	    }
	    else {
	    try {	
	      if (mediaController != null) {
	    	    mediaController.superhide();  
	        	mediaController = null;  
	          }
          if (mediaPlayer != null) {
        	  mediaPlayer.reset();
        	  mediaPlayer.release();
        	  mediaPlayer = null;
          }

         // Log.d("com.voiceshot.vsdialer", "vsdialer new MediaPlayer()"); 
	      mediaPlayer = new MediaPlayer();    
	    //  Log.d("com.voiceshot.vsdialer", "vsdialer new VMMediaController(this,false);"); 
	      mediaController = new VMMediaController(this,false);
	    //  Log.d("com.voiceshot.vsdialer", "vsdialer setMediaPlayer"); 
	      mediaController.setMediaPlayer(boxActivity.this); 	      	      	

	      
	      mediaController.setPrevNextListeners(new OnClickListener() {
	          @Override
	          public void onClick(View v) {
	        	  mediaPlayer.seekTo(mediaPlayer.getDuration()-2);
	        	  if (!mediaPlayer.isPlaying()){
	        		  mediaController.show();	        		  
	        	  }      	  
	          }
	      }, new OnClickListener() {
	          @Override
	          public void onClick(View v) {
	        	  mediaPlayer.seekTo(0);
	        	  if (!mediaPlayer.isPlaying()){
	        		  mediaController.show();
	        	  }
	          }
	      });	      
	      mediaPlayer.setOnPreparedListener(this);    	      
	      mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() 
	      {           
	          public void onCompletion(MediaPlayer mp) 
	          {	        	  
	        	  mediaPlayer.reset();
	        	  mediaPlayer.release();
	        	  if (mediaController != null) {
	                mediaController.superhide();
	                mediaController = null;
	        	  }
	        	  mediaPlayer = null;             
	          }           
	      });       
	      //Log.d("com.voiceshot.vsdialer", "vsdialer setOnCompletionListener done"); 
	     
	    
	       if (!currentFileName.equalsIgnoreCase(curfilename+".wav")) {	  
    	     if (fileExists(currentFileName)) {			   
	   		    File tmpFile = new File(getFilesDir()+currentFileName);
	   		    tmpFile.delete();						   
	   		  }
	       }
    	    currentFileName = curfilename+".wav";
    	    //Log.d("com.voiceshot.vsdialer", "vsdialer setDataSource"); 
	        mediaPlayer.setDataSource(AUDIO_FILE_NAME);
	        //Log.d("com.voiceshot.vsdialer", "vsdialer prepare"); 
	        mediaPlayer.prepare();
	        //Log.d("com.voiceshot.vsdialer", "vsdialer start"); 
	        mediaPlayer.start();   
	     //   mediaController.setAnchorView((View) v);
	       // Log.d("com.voiceshot.vsdialer", "vsdialer setAnchorView"); 
	        mediaController.setAnchorView((View) v);	        
	      //  Log.i(TAG, "Here " );
	      } catch (Exception e) {   // was IOException	    	
		      if (mediaController != null) {
		    	    mediaController.superhide();  
		        	mediaController = null;  
		          }
	          if (mediaPlayer != null) {
	        	  mediaPlayer.reset();
	        	  mediaPlayer.release();
	        	  mediaPlayer = null;
	          }	    	  
     	      if (fileExists(curfilename+".wav")) {     	    	
 	   		    File tmpFile = new File(getFilesDir()+curfilename+".wav");
 	   		    if (!tmpFile.delete()) {
 	   		    //Log.d("com.voiceshot.vsdialer", "vsdialer file not deleted - Exception"); 
 	   		    }
     	      } else {     	   	  
	    	    Toast.makeText(getApplicationContext(),"Could not open file " + AUDIO_FILE_NAME + " for playback." + e.toString(), Toast.LENGTH_SHORT).show();
     	      }
	      }  
	  }	
	  }
	}
    
    
    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private View curView;
        private String curFilename;
        private Activity activity;        

        public DownloadTask(Context context, View v, String FN, Activity a) {
            this.context = context;
            this.curView = v;
            this.curFilename = FN;
            this.activity = a;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            // take CPU lock to prevent CPU from going off if the user 
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                 getClass().getName()); 
            wl.acquire();
            //Log.d("com.voiceshot.vsdialer", "vsdialer doInBackground"); 
            try {
                InputStream input = null;
                OutputStream output = null;
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(sUrl[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    //Log.d("com.voiceshot.vsdialer", "vsdialer connection.connect"); 
                    // expect HTTP 200 OK, so we don't mistakenly save error report 
                    // instead of the file
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                         return "Server returned HTTP " + connection.getResponseCode() 
                             + " " + connection.getResponseMessage();

                    // this will be useful to display download percentage
                    // might be -1: server did not report the length
                    int fileLength = connection.getContentLength(); 

                    // download the file
                    input  = connection.getInputStream();
                    output = new FileOutputStream(getFilesDir()+curFilename); 

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        // allow canceling with back button
                        if (isCancelled()){
                        	//Log.d("com.voiceshot.vsdialer", "vsdialer input.read cancelled"); 
                        	output.close();
                        	output = null;
                    		if (fileExists(getFilesDir()+curFilename)) {	
                    			//Log.d("com.voiceshot.vsdialer", "vsdialer input.read cancelled file exists"); 
                 			     File tmpFile = new File(getFilesDir()+curFilename);  
                 			     if (!tmpFile.delete()) {
                 			    	//Log.d("com.voiceshot.vsdialer", "vsdialer input.read cancelled file exists deleted failed");  
                 			     }
                 		    }                           	
                        	return null;
                        }
                            
                        total += count;
                        // publishing the progress....
                        if (fileLength > 0) // only if total length is known
                            publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                        //Log.d("com.voiceshot.vsdialer", "vsdialer input.read"); 
                    }
                   // Log.d("com.voiceshot.vsdialer", "vsdialer input.read done"); 
                } catch (Exception e) {
                    return e.toString();
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } 
                    catch (IOException ignored) { }

                    if (connection != null)
                        connection.disconnect();
                }
            } finally {
                wl.release();
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
    		if (fileExists(dlFileName)) {			    
  			     File tmpFile = new File(getFilesDir()+dlFileName);  
  			     tmpFile.delete();						   
  		    }               
            mProgressDialog.setMessage("Loading Voicemail");
            mProgressDialog.show();
            mProgressDialog.setProgress(0);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
        	//Log.d("com.voiceshot.vsdialer", "vsdialer onPostExecute"); 
        	if(isCancelled()) {
        		if (fileExists(dlFileName)) {			    
   			     File tmpFile = new File(getFilesDir()+dlFileName);  
   			     tmpFile.delete();						   
   		        }        		
        		return;
        	}
            mProgressDialog.dismiss();
           // Toast.makeText(context,"onPostExecute finish", Toast.LENGTH_SHORT).show();
            if(!isCancelled()) {
              if (result != null)
                  Toast.makeText(context,"Unable to download", Toast.LENGTH_LONG).show();
              else {
                   ((boxActivity)activity).listenToSpeakerVMClick(curView);
              }
            }
            downloadTask = null;
        }        
    }  
    
    
   
    
    
    private class DeleteVMTask extends AsyncTask<String, Integer, String> {

        private Context context;	        
        private String curFilename;
        private Activity activity;

        public DeleteVMTask(Context context, String FN, Activity a) {
            this.context = context;
            this.curFilename = FN;
            this.activity = a;
        }
        
        @Override
        protected String doInBackground(String... sUrl) {
           HttpURLConnection connection = null; 
           try {
              URL url = new URL(sUrl[0]);
              connection = (HttpURLConnection) url.openConnection();
              connection.connect();
  
              if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                  return "Server returned HTTP " + connection.getResponseCode() 
                      + " " + connection.getResponseMessage();
           }
           catch (IOException ignored) { }
           if (connection != null) connection.disconnect();               
	          return null;	
        }
        
        @Override
        protected void onPostExecute(String result) {
           //
        } 	        
    }
    
    private class SaveVMTask extends AsyncTask<String, Integer, String> {

        private Context context;	        
        private String curFilename;
        private Activity activity;

        public SaveVMTask(Context context, String FN, Activity a) {
            this.context = context;
            this.curFilename = FN;
            this.activity = a;
        }
        
        @Override
        protected String doInBackground(String... sUrl) {
           HttpURLConnection connection = null; 
           try {
              URL url = new URL(sUrl[0]);
              connection = (HttpURLConnection) url.openConnection();
              connection.connect();
  
              if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                  return "Server returned HTTP " + connection.getResponseCode() 
                      + " " + connection.getResponseMessage();
           }
           catch (IOException ignored) { }
           if (connection != null) connection.disconnect();               
	          return null;	
        }
        
        @Override
        protected void onPostExecute(String result) {
           //
        } 	        
    }    
    
     
    
    
}
