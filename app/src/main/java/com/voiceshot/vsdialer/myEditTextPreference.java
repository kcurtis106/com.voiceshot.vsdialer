package com.voiceshot.vsdialer;


import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;




public class myEditTextPreference extends EditTextPreference {
    public myEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);           
    }

    public myEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public myEditTextPreference(Context context) {
        super(context);       
    }
    
    public void show() {
        showDialog(null);
    }    
}
