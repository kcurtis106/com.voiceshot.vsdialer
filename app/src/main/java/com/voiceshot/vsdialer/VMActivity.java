package com.voiceshot.vsdialer;

import com.voiceshot.vsdialer.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceChangeListener;
import android.telephony.PhoneNumberUtils;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class VMActivity extends android.preference.PreferenceActivity implements OnSharedPreferenceChangeListener,OnPreferenceChangeListener {
	private EncEditTextShowPINDialog mEncVMPINDialog;
	private EditText mEditText;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.vmactivity); 
        mEncVMPINDialog = (EncEditTextShowPINDialog) findPreference(getString(R.string.vm_pin_preferences));
        mEncVMPINDialog.setOnPreferenceChangeListener(this);   
        try {
        	mEncVMPINDialog.setText(SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(getBaseContext())).getString(getString(R.string.vm_pin_preferences),"")));
        } catch (Exception e) { e.printStackTrace();  }
        mEncVMPINDialog.setCompoundButtonListener(new View.OnClickListener()  
        {			              	     
        	public void onClick(View v) 
        	{             
        		if (mEncVMPINDialog.isShowPINChecked()) {
        			mEncVMPINDialog.setEditTransformationMethod(null);        		
        		}
        		else {
        			mEncVMPINDialog.setEditTransformationMethod(PasswordTransformationMethod.getInstance()) ;        			
        		}
        	}  
        } );  
           
        
        
        for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++){
        	initSummary(getPreferenceScreen().getPreference(i));              
        }           
    }
	
	
	public boolean onPreferenceChange(Preference preference, Object newValue) {		
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	try {
    		Toast.makeText(getApplicationContext(), "Value (VMActivity) for "+preference.getKey().toString()+" "+newValue.toString(), Toast.LENGTH_LONG).show();
    		if (preference.getKey().equalsIgnoreCase(getString(R.string.dd_pin_preferences))){ 	
    			
    	      prefs.edit().putString(getString(R.string.dd_pin_preferences),SimpleCrypto.encrypt(getString(R.string.vs_enc_pw), newValue.toString())).commit();
    		}
    		else if (preference.getKey().equalsIgnoreCase(getString(R.string.vm_pin_preferences))){ 
    		  prefs.edit().putString(getString(R.string.vm_pin_preferences),SimpleCrypto.encrypt(getString(R.string.vs_enc_pw), newValue.toString())).commit();	
    		}
        } catch (Exception e) { e.printStackTrace();  }
    	//Log.i("Preferences",prefs.getString(getString(R.string.dd_pin_preferences), ""));
    	updatePrefSummary(preference);
    	return true;				
	}

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    	Toast.makeText(getApplicationContext(), "Shared Value (VMActivity) for "+key.toString(), Toast.LENGTH_LONG).show();
    	updatePrefSummary(findPreference(key));         
    }  
	
    @Override          
    protected void onResume(){
    	super.onResume();             // Set up a listener whenever a key changes
    	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);     			
    	
    }
    
    @Override          
    protected void onPause() { 
    	super.onPause();             // Unregister the listener whenever a key changes
    	getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this); 
    }  
    
    private void updatePrefSummary(Preference p){
    	if (p instanceof ListPreference) {
    		ListPreference listPref = (ListPreference) p;
    		p.setSummary(listPref.getEntry());              
    	}
    	if (p instanceof EditTextPreference) {     		    		
    		EditTextPreference editTextPref = (EditTextPreference) p;     
    		if (p.getKey().equalsIgnoreCase(getString(R.string.dd_pin_preferences))){
    			String tmpStr = new String(new char[editTextPref.getText().length()]).replace('\0', '*'); 
    			p.setSummary(tmpStr);
    			
    		}
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.vs_phone_preferences))) {
    			if (editTextPref.getText().length() > 0) {
    			  p.setSummary(PhoneNumberUtils.formatNumber(editTextPref.getText()));
    			}
    			else {
    			  p.setSummary("Touch here to set your number");	     				
    			}
    		}
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.vm_box_preferences))) {
    			if (editTextPref.getText().length() > 0) {
    			  p.setSummary(PhoneNumberUtils.formatNumber(editTextPref.getText()));
    			}
    			else {
    			  p.setSummary("Touch here to set VM box number");	     				
    			}
    		}    		
    		else {
    		 p.setSummary(editTextPref.getText());
    		}	
    	}  
    	if (p instanceof EncEditTextShowPINDialog) { 
    		//EncEditTextShowPINDialog editTextPref = (EncEditTextShowPINDialog) p;     
    		if (p.getKey().equalsIgnoreCase(getString(R.string.dd_pin_preferences))){ 
    		  String tmpStr = "";
    		  try {
    		    tmpStr = new String(new char[SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(getBaseContext())).getString(getString(R.string.dd_pin_preferences),"")).length()]).replace('\0', '*');
    		  } catch (Exception e) { e.printStackTrace();  }
    		  if (tmpStr.length() > 0){
    		    p.setSummary(tmpStr);
    		  }
    		  else { 
    			p.setSummary("Touch here to set your PIN");
    	      }    		  
    		}
    		else if (p.getKey().equalsIgnoreCase(getString(R.string.vm_pin_preferences))){ 
      		  String tmpStr = "";
      		  try {
      		    tmpStr = new String(new char[SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(getBaseContext())).getString(getString(R.string.vm_pin_preferences),"")).length()]).replace('\0', '*');
      		  } catch (Exception e) { e.printStackTrace();  }
      		  if (tmpStr.length() > 0){
      		    p.setSummary(tmpStr);
      		  }
      		  else { 
      			p.setSummary("Touch here to set your VM PIN");
      	      } 
    		}
    	}
   	}     
 
    private void initSummary(Preference p){            
    	if (p instanceof PreferenceCategory){                 
    		PreferenceCategory pCat = (PreferenceCategory)p; 
    		for(int i=0;i<pCat.getPreferenceCount();i++){
    			initSummary(pCat.getPreference(i));                 }  
    		}else{ 
    			updatePrefSummary(p);             } 
    }    

}