package com.voiceshot.vsdialer;



import static com.voiceshot.vsdialer.CommonUtilities.EXTRA_MESSAGE;
import static com.voiceshot.vsdialer.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.REGISTRATION_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.UNREGISTER_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.SENDER_ID;
import static com.voiceshot.vsdialer.CommonUtilities.SERVER_URL;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_RING_MESSAGE_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_ONE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_TWO;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_THREE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_FOUR;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.CommonUtilities.displayPhoneRingMessage;
import static com.voiceshot.vsdialer.CommonUtilities.showFor;
import static com.voiceshot.vsdialer.ServerUtilities.notifyVMbox;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

//import com.google.android.gcm.GCMRegistrar;
import com.voiceshot.vsdialer.ServerUtilities;
import com.voiceshot.vsdialer.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.LocalActivityManager;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.Toast;
import android.widget.TextView;
import android.view.MenuItem; 





public class DialNumbers extends TabActivity implements OnTabChangeListener{ 
    static final int PICK_CONTACT                  =  1;
    static final int PICK_LOG                      =  2;
    static final int PICK_OPTION                   =  3;
    static final int PICK_VM                       =  4;
    public static final int RESULT_CONTACT_BUTTON_PUSHED  =  Activity.RESULT_FIRST_USER + 1;
    public static final int RESULT_LOG_BUTTON_PUSHED      =  Activity.RESULT_FIRST_USER + 2;
    public static final int RESULT_OPTIONS_BUTTON_PUSHED  =  Activity.RESULT_FIRST_USER + 3;
    public static final int RESULT_DIAL_BUTTON_PUSHED     =  Activity.RESULT_FIRST_USER + 4;
    final static long INTERVAL=1950;
    final static long TIMEOUT=10000;
    TelephonyManager tm;
    String myMsg = "";
    Context thisContext;
    private ProgressDialog pDialog;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final int PERMISSIONS_REQUEST_CALL_PHONE = 101;
   
	private class DisplayToast implements Runnable{
        String mText;
        Context mContext;
        int duration;
        int screen_pos;

       public DisplayToast(String text, Context context,int _duration, int _screen_pos){
          mText = text;
          mContext = context;
          duration = _duration;
          screen_pos = _screen_pos;
        }

        public void run(){
          Toast myToast = Toast.makeText(mContext, mText, Toast.LENGTH_LONG);
          if (screen_pos != -1) {
            myToast.setGravity(screen_pos, 0, 0);
          }
          showFor(myToast,duration);
        }	
	}   


   private String vsDialNumber = "";
   TabHost tabHost;
   
   
   public void downloadData() {
	    pDialog = ProgressDialog.show(this, "Downloading Data..", "Please wait", true,false);
	   // Thread thread = new Thread(this);
	   // thread.start();
	}    

   
  
   /** Called when the activity is first created. */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState); 
    Log.i("onCreate","onCreate");
    try {
    getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
    setContentView(R.layout.tablayout); 
    ActionBar actionBar = getActionBar();    
    actionBar.setDisplayShowTitleEnabled(true);    
    actionBar.setTitle("VoiceShot");   
    
    actionBar.show();
    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    
    thisContext = this;
    tabHost = (TabHost) findViewById(android.R.id.tabhost);     
    
    tabHost.setup();
    TextView dview=new TextView(this); 
    dview.setText("Dial"); 
    TabSpec Dialspec = tabHost.newTabSpec("Dial"); 
    
    //Dialspec.setIndicator("Phone",getResources().getDrawable(R.drawable.stat_sys_speakerphone));
    Dialspec.setIndicator("DIALPAD");
    //Dialspec.setIndicator(dview);
    
    Intent DialIntent = new Intent(this, DialPage.class);
    
    Dialspec.setContent(DialIntent); 
    
    /*TabSpec Logspec = tabHost.newTabSpec("Log");
    
    //Logspec.setIndicator("Call Log",getResources().getDrawable(R.drawable.call_log2));
    Logspec.setIndicator("CALL LOG");
    TextView lview=new TextView(this); 
    lview.setText("Log");     
    //Logspec.setIndicator(lview);
    
    Intent LogIntent = new Intent(this, CallLogList.class);
    
    Logspec.setContent(LogIntent);   */
    
    TabSpec Contactspec = tabHost.newTabSpec("Contacts"); 
    
    //Contactspec.setIndicator("Contacts",getResources().getDrawable(R.drawable.sym_contact_card));
    Contactspec.setIndicator("CONTACTS");
    TextView cview=new TextView(this); 
    cview.setText("Contacts");        
    //Contactspec.setIndicator(cview);
    
    Intent ContactIntent = new Intent(this, blankTabAct.class);
    
    Contactspec.setContent(ContactIntent);
    
    
    TabSpec VMspec = tabHost.newTabSpec("VM"); 
    
    //VMspec.setIndicator("Voicemail",getResources().getDrawable(R.drawable.stat_notify_voicemail));
    VMspec.setIndicator("VOICEMAIL");
    TextView vview=new TextView(this); 
    vview.setText("VM");     
    //VMspec.setIndicator(vview);
    
    //04/28/17 change
   // Intent VMIntent = new Intent(this, VM_Box_Activity.class);
    Intent VMIntent = new Intent(this, TabGroupActivity.class);
    VMIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    VMspec.setContent(VMIntent);   
    
   /* TabSpec Settingsspec = tabHost.newTabSpec("Settings"); 
    Settingsspec.setIndicator("Settings",null);
    Intent SettingsIntent = new Intent(this, Preferences.class);
    Settingsspec.setContent(SettingsIntent);*/ 
    
    
    tabHost.addTab(Dialspec);
    
    //tabHost.addTab(Logspec);
    
    tabHost.addTab(Contactspec);
    
    tabHost.addTab(VMspec);
    final TabWidget tw = tabHost.findViewById(android.R.id.tabs);
    for (int i = 0; i < tw.getChildCount(); ++i)
    {
        final View tabView = tw.getChildTabViewAt(i);              
        final TextView tv = tabView.findViewById(android.R.id.title);
        tabView.setBackgroundResource(R.layout.tab_indicator_ab);       
        tv.setTextSize(10);
        tv.setTextColor(Color.WHITE);                
    }      
        
    getTabWidget().getChildAt(2).setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
        	if (tabHost.getCurrentTabTag()=="VM") {
        	 // LocalActivityManager manager = getLocalActivityManager();
        	 // manager.destroyActivity("VM", true);        	
        	 // manager.startActivity("VM", VMIntent);
        		tabHost.setCurrentTab(0);
        		tabHost.setCurrentTab(2);
        	}else {
        		tabHost.setCurrentTab(2);
        	}
          
        }
    });    
        
    Intent startIntent = getIntent();
    
    Bundle bundle = startIntent.getExtras();
    
    if (bundle==null) {    	
        
    	tabHost.setCurrentTab(0);	
       
    }
    else {    	
      if(bundle.getString("which_tab")==null) {
      
    	  tabHost.setCurrentTab(0);  
      
    	  
      }else {
        if (bundle.getString("which_tab").contentEquals("vm_tab")) {        	         	 
    	  if (bundle.getString("which_box")!=null) {
    		  notifyVMbox        = bundle.getString("which_box");
    	  }
    	  tabHost.setCurrentTab(2);
        }
        else {        	 
    
          tabHost.setCurrentTab(0);
    
        }
     }
    }
    
    tabHost.setOnTabChangedListener(this);
    
    getOverflowMenu();   
    
    tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
    
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    
    boolean previouslyStarted = prefs.getBoolean("PREVIOUSLY_STARTED", false);
    
      
    registerReceiver(mHandleMessageReceiver,  new IntentFilter(DISPLAY_MESSAGE_ACTION));
    
    registerReceiver(mHandlePhoneRingReceiver,  new IntentFilter(PHONE_RING_MESSAGE_ACTION));
    
    if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
    
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        //previouslyStarted = false;  //debug added by N
    if(!previouslyStarted){
    	/*startActivity(new Intent(this, Preferences.class));
        AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());

        LayoutInflater inflater = getCurrentActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.sign_in, null))
    	    // Add action buttons
    	           .setPositiveButton(R.string.sign_in, new DialogInterface.OnClickListener() {
    	               @Override
    	               public void onClick(DialogInterface dialog, int id) {
    	                   // sign in the user ...
    	               }
    	           })
    	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
    	               public void onClick(DialogInterface dialog, int id) {
    	            	  startActivity(new Intent(getCurrentActivity(), Preferences.class));
    	                  dialog.cancel();
    	               }
    	           });
    	   AlertDialog alert11 = builder.create();
    	   alert11.show();
    	    	    
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.sign_in);
        dialog.setTitle("VS Dialer sign in");
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();*/
        //TextView text = (TextView) dialog.findViewById(R.id.text1);
        //text.setText("Message");

	
        SharedPreferences.Editor edit = prefs.edit();
    
       // edit.putBoolean("PREVIOUSLY_STARTED", Boolean.TRUE);
    
        edit.putString(getString(R.string.pref_toast_position), "16");
        edit.putString(getString(R.string.pref_local_phone), ServerUtilities.onlyTheNumbers(ServerUtilities.getMyPhoneNumber(getBaseContext(), getCurrentActivity())));
        edit.commit();
    
        startActivity(new Intent(this, Preferences.class));
      
    } 
    } catch (Exception e){
    	Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
      }
  
  }      
    
  private final BroadcastReceiver mHandleMessageReceiver =
          new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
  	      	
          String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
          if (newMessage == null) {newMessage = "";}        
          Log.i(TAG, "mHandleMessageReceiver is setting current msg: "+ newMessage);
          if (newMessage.length() > 0) {myMsg = newMessage; }
          if( tm.getCallState() == TelephonyManager.CALL_STATE_RINGING) { displayPhoneRingMessage(context,"");}    
          else {
        	  //Toast.makeText(context, newMessage, Toast.LENGTH_LONG).show();
        	  DisplayToast tmpToast =  new DisplayToast(newMessage,getApplicationContext(),8500,-1);
        	  tmpToast.run();
          }
          
      }
  };  
  
  private final BroadcastReceiver mHandlePhoneRingReceiver =
          new BroadcastReceiver() {
	     @Override
	      public void onReceive(Context context, Intent intent) {
	    	 Log.i(TAG, "mHandlePhoneRingReceiver is attempting to display current msg" );
	          if (myMsg.length() > 0) {
	              	final Toast tag = Toast.makeText(getBaseContext(), myMsg,Toast.LENGTH_SHORT); 
	              	tag.show();
	                  
	                  TimerTask task=new TimerTask(){ 
	                      @Override
	                      public void run() { 
	                      	if( ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getCallState() != TelephonyManager.CALL_STATE_RINGING) {
	                      		this.cancel();
	                      		myMsg = "";
	                      	}
	                      	else {
	                      	  tag.show();
	                      	}
	                          //if(some other conditions)
	                          //   this.cancel();
	                         
	                      }
	                  };
	                  Timer timer = new Timer();
	                  timer.scheduleAtFixedRate(task, INTERVAL, INTERVAL);                	              		              
	           }	    	 
	     }
  };
  
  public void onTabChanged(String tabId) {
      /* Your code to handle tab changes */
	  
	        
	  if (tabHost.getCurrentTab() == 0) {
		    Intent i = new Intent();
		    i.setAction("com.voiceshot.dialnumbers.setDial");
		    i.putExtra(Intent.EXTRA_PHONE_NUMBER, vsDialNumber);
		    sendBroadcast(i);		    
		  
	  }else if (tabHost.getCurrentTab() == 1) {
		  tabHost.setCurrentTab(0);
		  Intent i = new Intent();
		  i.setAction("com.voiceshot.dialnumbers.setDial");
		  String tmpNum = "-1";
		  i.putExtra(Intent.EXTRA_PHONE_NUMBER, tmpNum);		  
		  sendBroadcast(i);		  
	  }
	  vsDialNumber = "";
  }
  
 
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
	 // Log.i("onCreateOptionsMenu",menu.toString());
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.activity_dial_numbers, menu);	  
	  return super.onCreateOptionsMenu(menu);
	 // return true;
  }

  
  
 
  
 @Override 
 public boolean onPrepareOptionsMenu(Menu menu) {    
    // Intent settingsActivity = new Intent(getBaseContext(),Preferences.class);
    // startActivityForResult(settingsActivity,PICK_OPTION);     
	 return super.onPrepareOptionsMenu(menu); 
	// return true; 
    // return false;
} 
 
 @Override
 public boolean onOptionsItemSelected(MenuItem item) {
     // Handle item selection
     Intent settingsActivity = new Intent(getBaseContext(),Preferences.class);
     startActivityForResult(settingsActivity,PICK_OPTION);    
         return super.onOptionsItemSelected(item);
 }    
 
 public void setCn (String inNumber){ 
	 inNumber = inNumber.replaceAll( "[^\\d]", "" );
	 ((EditText) findViewById(R.id.text_number)).setText("",TextView.BufferType.EDITABLE);
	 ((EditText) findViewById(R.id.text_number)).append(inNumber);
 }
  
  

  
    public void getContacts(){    	
        tabHost.setCurrentTab(1);
        //Intent intent = new Intent(Intent.ACTION_PICK);
    	//intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        //startActivityForResult(intent, PICK_CONTACT);
    }
    
    @Override public void onActivityResult(int reqCode, int resultCode, Intent data){ 
    	   super.onActivityResult(reqCode, resultCode, data);  
    	   switch(reqCode) {   
    	      case (PICK_CONTACT):      
    		     if (resultCode == Activity.RESULT_OK)      {
   	                Uri contactData = data.getData();
   	                Cursor cur = managedQuery(contactData, null, null, null, null);
   	                if (cur.moveToFirst()) {
   	                	String id = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
  				        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,        ContactsContract.CommonDataKinds.Phone._ID +" = "+ id,null, null);
  					    phones.moveToFirst();         
  					    String cNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));        
  					    Toast.makeText(getApplicationContext(), cNumber, Toast.LENGTH_SHORT).show();        
  					    setCn(cNumber);    	                	
                
   	                	id = null;                      
   	                	phones = null;                 
   	                }            
   	                cur = null;      		    	
    			} 
    	        break;
    	      case (PICK_LOG):
    				if (resultCode == Activity.RESULT_OK) {
    					String cNumber = data.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
    					setCn(cNumber);
    				}  
    				else if (resultCode == RESULT_CONTACT_BUTTON_PUSHED) {
    					getContacts();
    				}
      	    	  else if (resultCode == RESULT_OPTIONS_BUTTON_PUSHED) {

    	    	  }
    	          break;
    	      case (PICK_OPTION):
    	    	  if (resultCode == RESULT_CONTACT_BUTTON_PUSHED) {
    	    		  getContacts();  
    	    	  }
    	    	  else if (resultCode == RESULT_LOG_BUTTON_PUSHED) {

    	    	  }
    	         break;
    	      case (PICK_VM):
    	    	  if (resultCode == RESULT_CONTACT_BUTTON_PUSHED) {
    	    		  getContacts();  
    	    	  }
    	    	  else if (resultCode == RESULT_LOG_BUTTON_PUSHED) {

    	    	  }
    	         break;    	         
    		} 
    	}
    
    public void doDial(View view){ 
		String vsNumber = "";
		String ddPIN    = ""; 
	  	String PhoneNumber = ((EditText) findViewById(R.id.text_number)).getText().toString();
	  	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	  	vsNumber = ServerUtilities.onlyTheNumbers(prefs.getString(getString(R.string.vs_phone_preferences),""));
	  	try {  	  
	      ddPIN = SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),prefs.getString(getString(R.string.dd_pin_preferences),""));
	  	} catch (Exception e) { e.printStackTrace();  }
	    if((PhoneNumber.length() == 10) && (ddPIN.length() > 0) && (vsNumber.length() == 10) ){ 
	      try {
	    	  Log.i(TAG, PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO+ddPIN+PHONE_DIAL_ACTION_THREE + PhoneNumber + PHONE_DIAL_ACTION_FOUR);  
	        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO+ddPIN+PHONE_DIAL_ACTION_THREE + PhoneNumber + PHONE_DIAL_ACTION_FOUR)));
	      } catch (Exception e) {
	        e.printStackTrace();
	      }
	    }
	    else {
	      if (PhoneNumber.length() != 10) { 
	    	  Toast.makeText(getApplicationContext(), "Phone number must be 10 digits", Toast.LENGTH_LONG).show(); 
	      }
	      else if (ddPIN.length() == 0){
	    	  Toast.makeText(getApplicationContext(), "Outbound calling PIN cannot be blank!", Toast.LENGTH_LONG).show();
	      }
	      else {
	    	  Toast.makeText(getApplicationContext(), "Voiceshot number must be 10 digits", Toast.LENGTH_LONG).show();
	      }
	      }
	    }    
    
    
  
    
    public void addKey (View view) {
    	String inKey =  (String) view.getTag(); 
    	((EditText) findViewById(R.id.text_number)).append(inKey);

    }
    
    public void delKey (View view) {
    	KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
    	((EditText) findViewById(R.id.text_number)).dispatchKeyEvent(event);
	
    }

	public String getVsDialNumber() {
		return vsDialNumber;
	}

	public void setVsDialNumber(String invsDialNumber) {
		vsDialNumber = invsDialNumber;
	}    
	
    @Override
    protected void onDestroy() {               
        unregisterReceiver(mHandleMessageReceiver); 
        unregisterReceiver(mHandlePhoneRingReceiver);
        //GCMRegistrar.onDestroy(this);
        super.onDestroy();
    }	
    
    private void getOverflowMenu() {

        try {
           ViewConfiguration config = ViewConfiguration.get(this);
           Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
           if(menuKeyField != null) {
               menuKeyField.setAccessible(true);
               menuKeyField.setBoolean(config, false);
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
     }    
    
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }    
    
    @Override          
    protected void onResume(){
    	super.onResume();              	   			
        Intent startIntent = getIntent();
        Bundle bundle = startIntent.getExtras();
        if (bundle!=null)  {
          if(bundle.getString("which_tab")!=null) {
            if (bundle.getString("which_tab").contentEquals("vm_tab")) {
        	  startIntent.putExtra("which_tab",""); 
        	  if (bundle.getString("which_box")!=null) {
        		  notifyVMbox        = bundle.getString("which_box");
        	  }        	  
        	  tabHost.setCurrentTab(2);
            }
          }
        }    	
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                getContacts();
            } else {
                Toast.makeText(this, "Please grant VoiceShot dialer permission to display your contacts", Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == PERMISSIONS_REQUEST_CALL_PHONE) {
            if ((grantResults.length == 0) || (grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(this, "Please grant VoiceShot dialer permission to dial your number", Toast.LENGTH_SHORT).show();
            }
        }
    }

        
  }