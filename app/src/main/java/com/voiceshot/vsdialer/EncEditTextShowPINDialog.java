package com.voiceshot.vsdialer;
import com.voiceshot.vsdialer.R;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.DialogPreference;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.text.Spanned;


public class EncEditTextShowPINDialog extends DialogPreference {
	private EditText mEditText;
	private CheckBox mCheckBox;
	private String mText = "";
	private View.OnClickListener mCheckBoxCallback;
	private Context curContext;
	
	public EncEditTextShowPINDialog(Context context, AttributeSet attrs, int defStyle) {
		  
		  super(context, attrs, defStyle);    
		  curContext = context;
		  setDialogLayoutResource(R.layout.encedittextshowpin);  		  
	  }	

	  public EncEditTextShowPINDialog(Context context, AttributeSet attrs) { 
		  
		  super(context, attrs); 
		  curContext = context;
		  setDialogLayoutResource(R.layout.encedittextshowpin);  
	  }	
	  
	 @Override
	 protected View onCreateDialogView() {  
		 View root = super.onCreateDialogView();  
		 mEditText = (EditText) root.findViewById(R.id.editPIN);  
		 mCheckBox = (CheckBox) root.findViewById(R.id.checkShowPIN); 
		 mEditText.setInputType(InputType.TYPE_CLASS_PHONE);  
		 InputFilter filter = new InputFilter() {
			    @Override
			    public CharSequence filter(CharSequence source, int start, int end,
			            Spanned dest, int dstart, int dend) {

			        if (source instanceof SpannableStringBuilder) {
			            SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder)source;
			            for (int i = end - 1; i >= start; i--) { 
			                char currentChar = source.charAt(i);
			                 if (!Character.isDigit(currentChar) && !(currentChar==',')) {    
			                     sourceAsSpannableBuilder.delete(i, i+1);
			                 }     
			            }
			            return source;
			        } else {
			            StringBuilder filteredStringBuilder = new StringBuilder();
			            for (int i = 0; i < end; i++) { 
			                char currentChar = source.charAt(i);
			                if (Character.isDigit(currentChar) || (currentChar==',')) {    
			                    filteredStringBuilder.append(currentChar);
			                }     
			            }
			            return filteredStringBuilder.toString();
			        }
			    }
			}; 	
		 mEditText.setFilters(new InputFilter[]{filter});		 
		 mEditText.setTransformationMethod(PasswordTransformationMethod.getInstance()); 

		 

		 return root;
	 }	  
	     
	 
	 public void setText(String text) {		 
		   mText = text;		 
	 }
	 
	 public String getText() {
		 		 
		 return mText;
	 }	 
	 
	 @Override
	 public void onClick(DialogInterface dialog, int which) { 
		 switch(which) {    
		   case DialogInterface.BUTTON_POSITIVE: // User clicked OK!      
			   setText(mEditText.getText().toString());      
			   callChangeListener(mText);    			   
			   break;  
			 }  
		 super.onClick(dialog, which);		 
	 }
	 
	 @Override
	 protected void onBindDialogView(View view) {
		 mCheckBox.setOnClickListener(mCheckBoxCallback);
		 mEditText.setText("");
		 mEditText.append(getText());		 		 		 		
	       (new Handler()).postDelayed(new Runnable() {

	            public void run() {

                    
	            	mEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN ,0, 0, 0));
	            	mEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0));                       	            	

	            }
	        }, 100);  
		 
	        
	 }	 
	 
	 public void setCompoundButtonListener(View.OnClickListener callback) {
		 mCheckBoxCallback = callback;
	 }	 
	 
	 public void setEditTransformationMethod (TransformationMethod method) {
		 mEditText.setTransformationMethod(method);	
	 }
	 
	 public boolean isShowPINChecked () {
	   return mCheckBox.isChecked();
	 }
	 
		
}
