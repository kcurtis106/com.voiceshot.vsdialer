package com.voiceshot.vsdialer;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.voiceshot.vsdialer.R;

import static com.voiceshot.vsdialer.CommonUtilities.SERVER_URL;
import static com.voiceshot.vsdialer.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.EXTRA_MESSAGE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_ONE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_TWO_VM;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_THREE;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_TWO;
import static com.voiceshot.vsdialer.CommonUtilities.PHONE_DIAL_ACTION_FOUR;
import static com.voiceshot.vsdialer.CommonUtilities.REMOVE_BOX_ACTION;
import static com.voiceshot.vsdialer.CommonUtilities.TAG;
import static com.voiceshot.vsdialer.CommonUtilities.DISPLAY_REFRESH_NEW_VM;
import static com.voiceshot.vsdialer.CommonUtilities.displayPhoneRingMessage;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPhone;
import static com.voiceshot.vsdialer.ServerUtilities.getNickName;
import static com.voiceshot.vsdialer.ServerUtilities.getRegID;
import static com.voiceshot.vsdialer.ServerUtilities.getSharedPIN;
import static com.voiceshot.vsdialer.ServerUtilities.getDNIS;
import static com.voiceshot.vsdialer.ServerUtilities.setCurVM;
import static com.voiceshot.vsdialer.ServerUtilities.getCurVM;
import static com.voiceshot.vsdialer.ServerUtilities.setOldVM;
import static com.voiceshot.vsdialer.ServerUtilities.getOldVM;
import static com.voiceshot.vsdialer.ServerUtilities.setOldPIN;
import static com.voiceshot.vsdialer.ServerUtilities.getOldPIN;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import com.voiceshot.vsdialer.VMDBProvider;

import android.widget.SimpleCursorAdapter;


class ImageAndTextAdapter extends ArrayAdapter<String> {

    private LayoutInflater mInflater;
        
          
    private int mViewResourceId;
    
    public ImageAndTextAdapter(Context ctx, int viewResourceId,
    		ArrayList<String> strings) {
        super(ctx, viewResourceId, strings);
        
        mInflater = (LayoutInflater)ctx.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
                      
        mViewResourceId = viewResourceId;
    }





    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);
        
        TextView tv = (TextView)convertView.findViewById(R.id.label);
        tv.setText(" " + getItem(position));
        
        return convertView;
    }
}

class MySimpleCursorAdapter extends SimpleCursorAdapter   {
	private Context appContext;	
    public MySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from,int[] to) {
        super(context, layout, c, from, to);
        this.appContext=context; 
    }
    
   
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);    	
        Cursor cursor = (Cursor) getItem(position);        
        ContactResult cr = CommonUtilities.getContactName(appContext,cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
        ImageView b = (ImageView) view.findViewById(R.id.list_image);
        TextView phTV = (TextView) view.findViewById(R.id.notify_phone_number);
        TextView midTV = (TextView) view.findViewById(R.id.notify_extra_info);  
        ImageView phoneIcon = (ImageView) view.findViewById(R.id.imageView1);        
        phoneIcon.setImageResource(R.drawable.ic_call_rotate);
        if (cr.getContactName() != null){        	
            if (cr.getContactType() != null){
            	phTV.setText( cr.getContactName());
            	midTV.setVisibility(View.VISIBLE); 
            	midTV.setText(cr.getContactType() + " " + cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
            }
            else {
            	phTV.setText(cr.getContactName());	
            	midTV.setVisibility(View.GONE);
            	midTV.setText("");
            }
        }
        else {
        	if (cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)).length() == 0) {
        		phTV.setText("Private Caller");      
        		phoneIcon.setImageResource(R.drawable.ic_call_rotate_grey);
        	}
        	else {        		        	
        	  phTV.setText(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
        	}
        	midTV.setVisibility(View.GONE);
        	midTV.setText("");
        }
        
        TextView dtTV = (TextView) view.findViewById(R.id.notify_date_time);
        dtTV.setText(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_TIMESTAMP)));
        
        TextView durTV = (TextView) view.findViewById(R.id.notify_duration);
        durTV.setText(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_DURATION)));

        
      //  TextView BoxTV = (TextView) view.findViewById(R.id.notify_vm_box);
      //  BoxTV.setText("Box: " +cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_BOX)));
        
      /*  if (cr.getContactID() != null){
          String uri = CommonUtilities.getPhotoUri(appContext,cr.getContactID());	          
          if (uri != null) {
        	  Uri thumbUri= Uri.parse(uri);
        	  AssetFileDescriptor afd = null;
        	  try {
                  afd = appContext.getContentResolver().openAssetFileDescriptor(thumbUri, "r");
                  FileDescriptor fileDescriptor = afd.getFileDescriptor();              
            	  b.setImageBitmap(BitmapFactory.decodeFileDescriptor(fileDescriptor, null, null)) ;        		  
        	  }  catch (FileNotFoundException e) {
        		  b.setImageResource(R.drawable.ic_menu_allfriends);
        	  }
          }
          else {
        	 b.setImageResource(R.drawable.ic_menu_allfriends);
          }
          //b.invalidate();
        }  
        else {
      	 b.setImageResource(R.drawable.ic_menu_allfriends);
        } */   
        if (cr.getBitmap() != null) {
        	b.setImageBitmap(cr.getBitmap());
        } else {
        	b.setImageResource(R.drawable.ic_play_circle2);
        }

        return view;
    }    
    

}



public class VM_Box_Activity  extends Activity implements OnPreparedListener, MediaController.MediaPlayerControl {
	
	public class MyMediaController extends MediaController {

	    public MyMediaController(Context context, boolean useFastForward) {
	        super(context, useFastForward);

	    }

	    @Override
	    public void hide() {
	        //mediaController.show(0);
	    }
	    
	    public void superhide() {
	    	super.hide();
	    }       
	    

	}   
	
	
	private ArrayList<String> boxArray = new ArrayList<String>();
	private ArrayList<String> pinArray = new ArrayList<String>();
	MediaPlayer mediaPlayer;
	private MyMediaController mediaController; 	

	private ListView list;
	private ListView notifyList;
	private View textEntryView;
    private Dialog newDialog;
    private ArrayAdapter<String> adapter;    
    private int curPos  = -1;     
    private VMDBProvider mDbHelper;
    private SimpleCursorAdapter VMList;
    private Cursor VMCursor;
    public String currentFileName = "";
    static final int DELETE_VM_BOX_MENU = 1;
    static final int EDIT_VM_BOX_MENU = 0;
    static final int DELETE_VM_MENU = 2;    
    private Handler handler = new Handler(); 
    ProgressDialog mProgressDialog;  
    
    public void removeBoxFromScreen(String inBox, String oldBox, String oldPIN) {
    	int curPos = -1;
    	for (int i = 0; i < boxArray.size(); i++) {
    		String curBox = boxArray.get(i);
    		if (curBox.contentEquals(inBox)){
    			curPos = i;
    		}
    	}
    	if (curPos != -1) {
    	  if (oldBox.length() > 0) {
    		boxArray.set(curPos, oldBox);
    		pinArray.set(curPos, oldPIN);
    		setOldVM(getBaseContext(),"");
    		setOldPIN(getBaseContext(),"");
    		setVMBoxes(true);
    	  } else {
    	    boxArray.remove(curPos);
    	    pinArray.remove(curPos);    	  
		    setVMBoxes(false);
    	  }
		  adapter.notifyDataSetChanged();     	  
    	}
    }
	
	public void onCreate(Bundle savedInstanceState) { 
        super.onCreate(savedInstanceState); 
        getVMBoxes();
		setContentView(R.layout.vmsettings);
		LayoutInflater factory = LayoutInflater.from(getBaseContext());  // getApplicationContext()
		View header = getLayoutInflater().inflate(R.layout.vm_header, null);

	    textEntryView = factory.inflate(R.layout.vm_imput_edit_layout, null);		
	    list = (ListView)findViewById(R.id.vm_list);	 
	    notifyList = (ListView)findViewById(R.id.vm_notify_list);
	    list.addHeaderView(header);
	    
	    //adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, boxArray);
	    adapter = new ImageAndTextAdapter(this,R.layout.vm_listview_row, boxArray);
	    list.setAdapter(adapter);	    
        mDbHelper = new VMDBProvider(this);
        mDbHelper.open();       
	    registerForContextMenu(list);
	    
	    list.setOnItemClickListener(new OnItemClickListener()
        {
       	    	
	    	public void onItemClick(AdapterView<?> arg0, View view,
                    int position, long id){
	    		//get selected items
	    		//String selectedValue = list.getItemAtPosition(position).toString();
	    		//Toast.makeText(getApplicationContext(), selectedValue, Toast.LENGTH_SHORT).show();
	    		doVMDial(boxArray.get(position-1),pinArray.get(position-1)); 	    		
	    	}
	    });
	    
	    
	    mProgressDialog = new ProgressDialog(VM_Box_Activity.this);
	    mProgressDialog.setMessage("Loading Voicemail");
	    mProgressDialog.setIndeterminate(true);
	    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	    mProgressDialog.setCancelable(true);	    

		  newDialog = new AlertDialog.Builder(VM_Box_Activity.this)	                
          .setTitle("Edit VoiceMail")
          .setView(textEntryView)
          .setPositiveButton("Save", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
                  String vmBox = ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).getText().toString();
                  String vmPIN = ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).getText().toString();
                  /* User clicked OK so do some stuff */
            	  if (curPos == -1) {
            		  boxArray.add(vmBox);
            		  pinArray.add(vmPIN);
            	  }
            	  else {            		  
              		  setOldVM(getBaseContext(),boxArray.get(curPos));
              		  setOldPIN(getBaseContext(),pinArray.get(curPos));            		  
            		  boxArray.set(curPos, vmBox); 
            		  pinArray.set(curPos, vmPIN);
            	  }
            	  ((CheckBox)textEntryView.findViewById(R.id.vmCheckShowPIN)).setChecked(false);
            	  ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setTransformationMethod(PasswordTransformationMethod.getInstance()) ;
            	  //dialog.dismiss();
            	  curPos  = -1;
            	  setCurVM(getBaseContext(),vmBox);
            	  setVMBoxes(true);            	  
            	  adapter.notifyDataSetChanged(); 
              }
          })
          .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
                     
                  /* User clicked cancel so do some stuff */
            	  dialog.cancel(); 
            	  curPos  = -1; 
              }
          })
          .create();
		  
	      
	        
		  InputFilter filter = new InputFilter() {
				    @Override
				    public CharSequence filter(CharSequence source, int start, int end,
				            Spanned dest, int dstart, int dend) {

				        if (source instanceof SpannableStringBuilder) {
				            SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder)source;
				            for (int i = end - 1; i >= start; i--) { 
				                char currentChar = source.charAt(i);
				                 if (!Character.isDigit(currentChar) && !(currentChar==',')) {    
				                     sourceAsSpannableBuilder.delete(i, i+1);
				                 }     
				            }
				            return source;
				        } else {
				            StringBuilder filteredStringBuilder = new StringBuilder();
				            for (int i = 0; i < end; i++) { 
				                char currentChar = source.charAt(i);
				                if (Character.isDigit(currentChar) || (currentChar==',')) {    
				                    filteredStringBuilder.append(currentChar);
				                }     
				            }
				            return filteredStringBuilder.toString();
				        }
				    }
		  }; 	
		  ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setFilters(new InputFilter[]{filter});    		  
        
		  CheckBox checkBox = (CheckBox)textEntryView.findViewById(R.id.vmCheckShowPIN);
		  
		  checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		     
		      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	        		if (isChecked) {
	        			((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setTransformationMethod(null);        		
	         		}
	         		else {
	         			((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setTransformationMethod(PasswordTransformationMethod.getInstance()) ;        			
	         		}
		      }
		  });
		  newDialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE); 
		  
		  
		  newDialog.setOnShowListener(new DialogInterface.OnShowListener() { 
			   
			    public void onShow(DialogInterface dialog) {

			        Button b = ((AlertDialog) newDialog).getButton(AlertDialog.BUTTON_POSITIVE);
			        b.setOnClickListener(new View.OnClickListener() {
			            
			            public void onClick(View view) {
  		                   String vmBox = ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).getText().toString();
			               String vmPIN = ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).getText().toString();			            	

			               if ((vmBox.length() == 0) || (vmPIN.length()==0)){
			            	   String itemName = "";
			            	   if (vmBox.length() == 0) {
			            		   itemName = "Voicemail Box";
			            	   }
			            	   else {
			            		   itemName = "PIN";
			            	   } 
			            	   Toast.makeText(getApplicationContext(), itemName + " cannot be blank!", Toast.LENGTH_LONG).show();  			            	   
			               }
			               else
			               {
			                   /* User clicked OK so do some stuff */ 
			             	  if (curPos == -1) {
			             		  boxArray.add(vmBox);
			             		  pinArray.add(vmPIN);
			             	  }
			             	  else {
			              		  setOldVM(getBaseContext(),boxArray.get(curPos));
			              		  setOldPIN(getBaseContext(),pinArray.get(curPos));  			             		  
			             		  boxArray.set(curPos, vmBox); 
			             		  pinArray.set(curPos, vmPIN);
			             	  }
			             	  ((CheckBox)textEntryView.findViewById(R.id.vmCheckShowPIN)).setChecked(false);
			             	  ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setTransformationMethod(PasswordTransformationMethod.getInstance()) ;
			             	  //dialog.dismiss();
			             	  curPos  = -1;
			             	  setCurVM(getBaseContext(),vmBox);
			             	  setVMBoxes(true);			             	  
			             	  adapter.notifyDataSetChanged(); 
			                  newDialog.dismiss();
			               }
			            }
			        });
			    }
			});		  
		      	
		  fillVMData();
	      notifyList.setOnItemClickListener(new OnItemClickListener()
	      {
	       	    	
		    	public void onItemClick(AdapterView<?> arg0, View view,
	                    int position, long id){		    		
                  //  mDbHelper.deleteVMEntry(id);
		    	  //  fillVMData();		  
		    	  //  Toast.makeText(getApplicationContext(), "ID: " + id + " Pos: " + position, Toast.LENGTH_SHORT).show(); 
		    	    listenToVMClick(view);
		    	}
		  });		      
	      registerReceiver(mHandleVMNotifyReceiver,  new IntentFilter(DISPLAY_REFRESH_NEW_VM));
	      registerReceiver(mHandleBoxReceiver,  new IntentFilter(REMOVE_BOX_ACTION));
	      registerForContextMenu(notifyList);	 
	      
	      
	      // REMOVE BEFORE POSTING TO STORE 
		 /*  for (int j =  0; j < VMList.getCount(); j++) {
			   Cursor tmpCursor = (Cursor) VMList.getItem(j);
			   String tmpfilename = tmpCursor.getString(tmpCursor.getColumnIndex(VMDBProvider.KEY_FILENAME))+".wav";
			   if (fileExists(tmpfilename)) {
				   File tmpFile = new File(getFilesDir()+tmpfilename);
				   if (tmpFile.delete()) {
					   Toast.makeText(getApplicationContext(), "deleted filename " + tmpfilename, Toast.LENGTH_SHORT).show();
				   }
				   else {
					 //  Toast.makeText(getApplicationContext(), "cannot delete filename " + tmpfilename, Toast.LENGTH_SHORT).show(); 
				   }
			   }
		   }	*/      
	}
	
	@Override
	public void onResume(){
	    super.onResume();
	    // put your code here...
	    boxArray = new ArrayList<String>();
	    getVMBoxes();
	    adapter = new ImageAndTextAdapter(this,R.layout.vm_listview_row, boxArray);
	    list.setAdapter(adapter);
	    adapter.notifyDataSetChanged();
	}	
	
	public void listenToSpeakerVMClick(View v) {
		
		listenToVMClick((View)v.getParent().getParent());
	}
	
	public void listenToVMClick(View v) {		
    	int position = notifyList.getPositionForView(v);	 
    	
    	Cursor cursor = (Cursor) VMList.getItem(position); 
    	String curfilename = cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_FILENAME));
    	//Toast.makeText(getApplicationContext(), "filename " + cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_FILENAME)), Toast.LENGTH_SHORT).show(); 
	    String AUDIO_FILE_NAME = getFilesDir()+curfilename+".wav";	
	    if (!fileExists(curfilename+".wav")) {
       	   final DownloadTask downloadTask = new DownloadTask(VM_Box_Activity.this,v,curfilename+".wav",this);
	       downloadTask.execute(SERVER_URL+"/vmdl.asp?fn="+URLEncoder.encode(curfilename)+"&nick="+URLEncoder.encode(getNickName(getApplicationContext()))+"&ph="+URLEncoder.encode(getSharedPhone(getApplicationContext()).replaceAll("\\s+",""))+"&regid="+URLEncoder.encode(getRegID(getApplicationContext())));

	        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
	          @Override
	          public void onCancel(DialogInterface dialog) {
	              downloadTask.cancel(true);
	              Toast.makeText(getApplicationContext(),"Download cancelled.", Toast.LENGTH_SHORT).show(); 
	          }
	      });
	    }
	    else {
	    try {	
	      if (mediaController != null) {
	    	    mediaController.superhide();  
	        	mediaController = null;  
	          }
          if (mediaPlayer != null) {
        	  mediaPlayer.reset();
        	  mediaPlayer.release();
        	  mediaPlayer = null;
          }
	      mediaPlayer = new MediaPlayer();    
	      mediaController = new MyMediaController(this,false);
	      mediaController.setMediaPlayer(VM_Box_Activity.this); 	      	      	

	      
	      mediaController.setPrevNextListeners(new OnClickListener() {
	          @Override
	          public void onClick(View v) {
	        	  mediaPlayer.seekTo(mediaPlayer.getDuration()-2);
	        	  if (!mediaPlayer.isPlaying()){
	        		  mediaController.show();	        		  
	        	  }      	  
	          }
	      }, new OnClickListener() {
	          @Override
	          public void onClick(View v) {
	        	  mediaPlayer.seekTo(0);
	        	  if (!mediaPlayer.isPlaying()){
	        		  mediaController.show();
	        	  }
	          }
	      });	      
	      mediaPlayer.setOnPreparedListener(this);    	      
	      mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() 
	      {           
	          public void onCompletion(MediaPlayer mp) 
	          {	        	  
	        	  mediaPlayer.reset();
	        	  mediaPlayer.release();
	        	  if (mediaController != null) {
	                mediaController.superhide();
	                mediaController = null;
	        	  }
	        	  mediaPlayer = null;             
	          }           
	      });       

	     
	    
	       if (!currentFileName.equalsIgnoreCase(curfilename+".wav")) {	  
    	     if (fileExists(currentFileName)) {			   
	   		    File tmpFile = new File(getFilesDir()+currentFileName);
	   		    tmpFile.delete();						   
	   		  }
	       }
    	    currentFileName = curfilename+".wav";
	        mediaPlayer.setDataSource(AUDIO_FILE_NAME);
	        mediaPlayer.prepare();
	     // mediaPlayer.setScreenOnWhilePlaying(true);  
	        mediaPlayer.start();   
	       // doRecurse(mediaController);
	        mediaController.setAnchorView((View) v);
	      //  Log.i(TAG, "Here " );
	      } catch (Exception e) {   // was IOException
	    	
	       // Log.e(TAG, "Could not open file " + AUDIO_FILE_NAME + " for playback." ,e);	    	  
	    	  Toast.makeText(getApplicationContext(),"Could not open file " + AUDIO_FILE_NAME + " for playback." + e.toString(), Toast.LENGTH_SHORT).show(); 
	      }	
	  }
	}
	
	public void doRecurse(View v) {
		if (v instanceof MediaController) {
			for (int i = 0; i < ((MediaController)v).getChildCount(); i++) {
				if (((MediaController)v).getChildAt(i) instanceof LinearLayout) {
					Log.i(TAG, "MediaController " +  ((MediaController)v).getChildAt(i));
					doRecurse(((MediaController)v).getChildAt(i));
				}
			}
		} else if (v instanceof LinearLayout) {
      	  for (int j = 0; j <  ((LinearLayout)v).getChildCount(); j++) {
      		Log.i(TAG, "LinearLayout Child " +  ((LinearLayout)v).getChildAt(j));
      		if (((LinearLayout)v).getChildAt(j) instanceof LinearLayout) {
      			doRecurse(((LinearLayout)v).getChildAt(j));
      		}
      	  }
		}
                		
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
	  if (v.getId()==R.id.vm_list) {
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
	    menu.setHeaderTitle("Box " +boxArray.get(info.position-1));
        menu.add(Menu.NONE, EDIT_VM_BOX_MENU, EDIT_VM_BOX_MENU, "Edit Box");
	    menu.add(Menu.NONE, DELETE_VM_BOX_MENU, DELETE_VM_BOX_MENU, "Remove Box");
	  }
	  else if (v.getId()==R.id.vm_notify_list) {		  	      
	      menu.setHeaderTitle("Delete" );	      	      
	      menu.add(Menu.NONE, DELETE_VM_MENU, DELETE_VM_MENU, "Voicemail");	 
	  } else {
		  Toast.makeText(getApplicationContext(),"Context Menu", Toast.LENGTH_SHORT).show(); 
	  }
	}	
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	//  int menuItemIndex = item.getItemId();
	//  String[] menuItems = new String[] { "Edit Box", "Delete Box"};
	//  String menuItemName = menuItems[menuItemIndex];
	 // String listItemName = boxArray.get(info.position-1);
	 // Toast.makeText(this, menuItemName + " " + listItemName, Toast.LENGTH_SHORT).show();
	  if ( item.getItemId() == EDIT_VM_BOX_MENU ) {
		  curPos = info.position-1;
		  ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).setText("",TextView.BufferType.EDITABLE);
		  ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).append(boxArray.get(info.position-1));
		  ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setText("",TextView.BufferType.EDITABLE);
		  ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).append(pinArray.get(info.position-1));		  
		  ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).requestFocus();
		  newDialog.setTitle("Edit VoiceMail");
		  newDialog.show();		  
		  
	  }
	  else if (item.getItemId() == DELETE_VM_BOX_MENU) {
		  boxArray.remove(info.position-1);
		  pinArray.remove(info.position-1);
		  setVMBoxes(true); 
		  adapter.notifyDataSetChanged(); 
	  }
	  else if (item.getItemId() == DELETE_VM_MENU) {
		  
		  if (isOnline()) {			 
		    Cursor cursor = (Cursor) VMList.getItem(info.position);	
		    String curfilename = cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_FILENAME));
		    if (fileExists(curfilename+".wav")) {			    
			     File tmpFile = new File(getFilesDir()+curfilename+".wav");  
			     tmpFile.delete();						   
		    }
		    mDbHelper.deleteVMEntry(cursor.getLong(0));
		    fillVMData();
		    DeleteVMTask deleteTask = new DeleteVMTask(VM_Box_Activity.this,curfilename+".wav",this);
		    deleteTask.execute(SERVER_URL+"/vmdel.asp?fn="+URLEncoder.encode(curfilename)+"&nick="+URLEncoder.encode(getNickName(getApplicationContext()).replaceAll("\\s+",""))+"&ph="+URLEncoder.encode(getSharedPhone(getApplicationContext()).replaceAll("\\s+",""))+"&regid="+URLEncoder.encode(getRegID(getApplicationContext())));			  
		  }
		  else {
			  Toast.makeText(getApplicationContext(), "Unable to delete voicemail message. Make sure data is enabled on your phone and your app settings are correct.", Toast.LENGTH_LONG).show();  
		  }
	  }
	  return true;
	}	 
	
    public void doVMDial(String vmBox,String vmPIN){ 		
		String vsNumber = "";
			  		  	
	  	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());	  	
	  	vsNumber = ServerUtilities.onlyTheNumbers(prefs.getString(getString(R.string.vs_phone_preferences),""));
	  	 if((vsNumber.length() == 10) && (vmPIN.length() > 0) && (vmBox.length() > 0) ){ 
	      try {
	    	Log.i(TAG,PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO_VM+vmBox+PHONE_DIAL_ACTION_THREE + vmPIN + PHONE_DIAL_ACTION_FOUR);
	        //startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+vsNumber+PHONE_DIAL_ACTION_TWO_VM+vmBox+PHONE_DIAL_ACTION_THREE + vmPIN + PHONE_DIAL_ACTION_FOUR)));
	    	new MyTask().execute(vmBox,vmPIN);

	      } catch (Exception e) {
	        e.printStackTrace();
	      }
	    }
	    else {
		  if (vsNumber.length() != 10) {
		    	  Toast.makeText(getApplicationContext(), "Phone number must be 10 digits", Toast.LENGTH_LONG).show(); 
		  }
	      else if (vmPIN.length() == 0){
	    	  Toast.makeText(getApplicationContext(), "Voicemail PIN cannot be blank!", Toast.LENGTH_LONG).show(); 
	      }
	      else {
	    	  Toast.makeText(getApplicationContext(), "Outbound calling number must be 10 digits", Toast.LENGTH_LONG).show();
	      }
	      }
	}
    
    public void addVMKey (View view) {
    	/* curPos = -1;
		  ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).setText("",TextView.BufferType.EDITABLE);
		  ((EditText) textEntryView.findViewById(R.id.vm_pin_edit)).setText("",TextView.BufferType.EDITABLE);
		  ((EditText) textEntryView.findViewById(R.id.vm_box_edit)).requestFocus();
		  ((CheckBox)textEntryView.findViewById(R.id.vmCheckShowPIN)).setChecked(false);		  
		  newDialog.setTitle("Add VoiceMail");
		  newDialog.show();		  */
    	getJSONData();
    }
    
    public void getVMBoxes () {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	int max = prefs.getInt("numboxes",0);
    	for (int i = 0; i < max; i++) {
    		boxArray.add(prefs.getString("box"+i,""));
    		try {
				pinArray.add(SimpleCrypto.decrypt(getString(R.string.vs_enc_pw),prefs.getString("pin"+i,"")));
			} catch (Exception e) {				 
				e.printStackTrace();
			}
    	}
    }
    
    public void setVMBoxes (boolean shouldRegister) { 
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	prefs.edit().putInt("numboxes",boxArray.size()).commit();
    	for (int i = 0; i < boxArray.size(); i++) {
    		prefs.edit().putString("box"+i,boxArray.get(i)).commit();
    		try {
				prefs.edit().putString("pin"+i,SimpleCrypto.encrypt(getString(R.string.vs_enc_pw), pinArray.get(i))).commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
    	} 
    	if (shouldRegister){
    	  if ((prefs.getString(getBaseContext().getString(R.string.pref_regid), "").length()>0)&&(prefs.getBoolean(getBaseContext().getString(R.string.pref_vm_notify_alert), false))) {
    	    ServerUtilities.register(getBaseContext(),prefs.getString(getBaseContext().getString(R.string.pref_regid), ""),prefs.getString(getBaseContext().getString(R.string.pref_regid), ""));
    	  }
    	}
    }
    
	
	  public boolean onKeyDown(int keyCode, KeyEvent event) {
		    if (keyCode == KeyEvent.KEYCODE_BACK) {
		    	(getParent()).moveTaskToBack(true);
		        return true;
		    }
		    else return super.onKeyDown(keyCode, event);
		    
		}	
	  
	    private void fillVMData() {	    		    		    	
	        VMCursor = mDbHelper.fetchAllVM();
	        startManagingCursor(VMCursor);
	        
	        // Create an array to specify the fields we want to display in the list (only TITLE)
	        String[] from = new String[]{VMDBProvider.KEY_PHONE,VMDBProvider.KEY_TIMESTAMP};
	        
	        // and an array of the fields we want to bind those fields to (in this case just text1)
	        int[] to = new int[]{R.id.notify_phone_number,R.id.notify_date_time};
	        
	        VMList =  new MySimpleCursorAdapter(this, R.layout.vm_notify_row, VMCursor, from, to);
	        notifyList = (ListView)findViewById(R.id.vm_notify_list);
	        notifyList.setAdapter(VMList);	        	        
	        
	    }	  
	    
	    public void phoneClick(View v) {	    	
	    	int position = notifyList.getPositionForView(v);	    	
        	Cursor cursor = (Cursor) VMList.getItem(position);   	        
        	if (cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)).length()!=0) {
    		  ((DialNumbers)getParent()).setVsDialNumber(cursor.getString(cursor.getColumnIndex(VMDBProvider.KEY_PHONE)));
    		  ((DialNumbers)getParent()).tabHost.setCurrentTab(0);
        	}
	     }	    
	    
	    private final BroadcastReceiver mHandleVMNotifyReceiver =
	            new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	        	fillVMData();		            
	        }
	    };	
	    
	    private final BroadcastReceiver mHandleBoxReceiver =
	            new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
        		String curBox = getCurVM(context);
        		String oldBox = getOldVM(context);
        		String oldPIN = getOldPIN(context);
        		if (curBox.length() > 0 ){
        			setCurVM(context,"");
        			removeBoxFromScreen(curBox,oldBox,oldPIN);
        		}	    	      		            
	        }
	    };	
	    
	    private void getJSONData() {
	    	DefaultHttpClient   httpclient = new DefaultHttpClient(new BasicHttpParams());
	    	Log.d(TAG,"https://api.voiceshot.com/gcm/vmGetList.asp?regid="+URLEncoder.encode(getRegID(getBaseContext()))+"&ph="+URLEncoder.encode(getSharedPhone(getBaseContext()))+"&nick="+URLEncoder.encode(getNickName(getBaseContext())));
	    	HttpGet httppost = new HttpGet("https://api.voiceshot.com/gcm/vmGetList.asp?regid="+URLEncoder.encode(getRegID(getBaseContext()))+"&ph="+URLEncoder.encode(getSharedPhone(getBaseContext()))+"&nick="+URLEncoder.encode(getNickName(getBaseContext())));

	    	InputStream inputStream = null;
	    	String result = null;
	    	try {
	    	    HttpResponse response = httpclient.execute(httppost);           
	    	    HttpEntity entity = response.getEntity();

	    	    inputStream = entity.getContent();
	    	    // json is UTF-8 by default
	    	    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
	    	    StringBuilder sb = new StringBuilder();

	    	    String line = null;
	    	    while ((line = reader.readLine()) != null)
	    	    {
	    	        sb.append(line);
	    	    }
	    	    result = sb.toString();
	    	} catch (Exception e) { 
	    	    // Oops
	    	}
	    	finally {
	    	    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
	    	}	
	    	mDbHelper.deleteALLVMEntry();
	    	try {
	    		Log.d(TAG,result);
				JSONObject jObject = new JSONObject(result);
				JSONArray items = jObject.getJSONArray("mailboxes");
				for (int i = 0; i < items.length(); i++) {
					JSONObject mailboxObject = items.getJSONObject(i);
					JSONArray newItems = mailboxObject.getJSONArray("newMessages");
					Log.d(TAG, mailboxObject.getString("box")+ " new messages(" +  mailboxObject.getString("newCount")+") saved messages("+ mailboxObject.getString("savedCount")+")");
					for (int j = 0; j < newItems.length(); j++) {
						JSONObject newMsgObj = newItems.getJSONObject(j);
						mDbHelper.createVMEntry(newMsgObj.getString("dateandtime"),newMsgObj.getString("callerid"),newMsgObj.getString("duration")+" secs",mailboxObject.getString("box"),newMsgObj.getString("uniqueid"),1);
					}
					JSONArray savedItems = mailboxObject.getJSONArray("savedMessages");					
					for (int j = 0; j < savedItems.length(); j++) {
						JSONObject savedMsgObj = savedItems.getJSONObject(j);
						mDbHelper.createVMEntry(savedMsgObj.getString("dateandtime"),savedMsgObj.getString("callerid"),savedMsgObj.getString("duration")+" secs",mailboxObject.getString("box"),savedMsgObj.getString("uniqueid"),0);
					}					
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	fillVMData();
	    }
	    
	    @Override
	    protected void onDestroy() {	    	
	    	unregisterReceiver(mHandleVMNotifyReceiver);
	    	unregisterReceiver(mHandleBoxReceiver); 
	    	if (fileExists(currentFileName)) {			   
	 	   	  File tmpFile = new File(getFilesDir()+currentFileName);
	 	   	  tmpFile.delete();		
	 	   	  currentFileName = "";
	 	    }	    	
	    	super.onDestroy();
	    }
	    	    
	    
	    //--MediaPlayerControl methods----------------------------------------------------
	    public void start() {
	      mediaPlayer.start();
	    }

	    public void pause() {
	      mediaPlayer.pause();
	     // mediaController.show(100);
	    }

	    public int getDuration() {
	      return mediaPlayer.getDuration();
	    }

	    public int getCurrentPosition() {
	      return mediaPlayer.getCurrentPosition();
	    }

	    public void seekTo(int i) {
	      mediaPlayer.seekTo(i); 
	    }

	    public boolean isPlaying() {
	      return mediaPlayer.isPlaying();
	    }

	    public int getBufferPercentage() {
	      return 0;
	    }

	    public boolean canPause() {
	      return true;
	    }

	    public boolean canSeekBackward() {
	      return true;
	    }

	    public boolean canSeekForward() {
	      return true;
	    }
	    //--------------------------------------------------------------------------------

	    @Override
	    protected void onStop() {
	      super.onStop();    
	      if (mediaController != null) {
	    	mediaController.superhide();  
	        mediaController = null;  
	      }
          if (mediaPlayer != null) {
      	    mediaPlayer.reset();
      	    mediaPlayer.release();
      	    mediaPlayer = null;
          }
	      if (fileExists(currentFileName)) {			   
		 	   File tmpFile = new File(getFilesDir()+currentFileName);
		 	   tmpFile.delete();		
		 	   currentFileName = "";
		  }	          
	    }

	   /* @Override
	    public boolean onTouchEvent(MotionEvent event) {
	      //the MediaController will hide after 3 seconds - tap the screen to make it appear again
	      mediaController.show();
	      return false; 
	    } */

	    public void onPrepared(MediaPlayer mediaPlayer) {
	        Log.d(TAG, "onPrepared");


	        handler.post(new Runnable() {
	          public void run() {
	            mediaController.setEnabled(true);
	            mediaController.show();
	           
	          }
	        });
	        
	      }

	   // @Override
	    public int getAudioSessionId() {
	    	// TODO Auto-generated method stub
	    	return 0;
	    }	    

	    public boolean fileExists(String filename) {
	    	File file = new File(getFilesDir()+filename);
	    	if(file.exists()) {
	    		return true;
	    	}
	    	else {
	    		return false;
	    	}
	    }
	    
	    @Override
	    public void onPause() {
	    	// TODO Auto-generated method stub
	    	//Toast.makeText(getApplicationContext(), "onPause ", Toast.LENGTH_SHORT).show();	
		    if (mediaController != null) {
			  	mediaController.superhide();  
			    mediaController = null;  
			}
		    if (mediaPlayer != null) {
		        mediaPlayer.reset();
		        mediaPlayer.release();
		        mediaPlayer = null;
		    }	    	
		    
		    if (fileExists(currentFileName)) {			   
			  File tmpFile = new File(getFilesDir()+currentFileName); 
			  tmpFile.delete();		
			  currentFileName = "";
			}		    
	    	super.onPause();
	    }		
	    
	    public boolean isOnline() {
	        ConnectivityManager cm =
	            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

	        return cm.getActiveNetworkInfo() != null && 
	           cm.getActiveNetworkInfo().isConnectedOrConnecting();
	    }	    
	    
	    private class DownloadTask extends AsyncTask<String, Integer, String> {

	        private Context context;
	        private View curView;
	        private String curFilename;
	        private Activity activity;

	        public DownloadTask(Context context, View v, String FN, Activity a) {
	            this.context = context;
	            this.curView = v;
	            this.curFilename = FN;
	            this.activity = a;
	        }

	        @Override
	        protected String doInBackground(String... sUrl) {
	            // take CPU lock to prevent CPU from going off if the user 
	            // presses the power button during download
	            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	                 getClass().getName());
	            wl.acquire();

	            try {
	                InputStream input = null;
	                OutputStream output = null;
	                HttpURLConnection connection = null;
	                try {
	                    URL url = new URL(sUrl[0]);
	                    connection = (HttpURLConnection) url.openConnection();
	                    connection.connect();

	                    // expect HTTP 200 OK, so we don't mistakenly save error report 
	                    // instead of the file
	                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
	                         return "Server returned HTTP " + connection.getResponseCode() 
	                             + " " + connection.getResponseMessage();

	                    // this will be useful to display download percentage
	                    // might be -1: server did not report the length
	                    int fileLength = connection.getContentLength(); 

	                    // download the file
	                    input  = connection.getInputStream();
	                    output = new FileOutputStream(getFilesDir()+curFilename); 

	                    byte data[] = new byte[4096];
	                    long total = 0;
	                    int count;
	                    while ((count = input.read(data)) != -1) {
	                        // allow canceling with back button
	                        if (isCancelled())
	                            return null;
	                        total += count;
	                        // publishing the progress....
	                        if (fileLength > 0) // only if total length is known
	                            publishProgress((int) (total * 100 / fileLength));
	                        output.write(data, 0, count);
	                    }
	                } catch (Exception e) {
	                    return e.toString();
	                } finally {
	                    try {
	                        if (output != null)
	                            output.close();
	                        if (input != null)
	                            input.close();
	                    } 
	                    catch (IOException ignored) { }

	                    if (connection != null)
	                        connection.disconnect();
	                }
	            } finally {
	                wl.release();
	            }
	            return null;
	        }
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            mProgressDialog.show();
	        }

	        @Override
	        protected void onProgressUpdate(Integer... progress) {
	            super.onProgressUpdate(progress);
	            // if we get here, length is known, now set indeterminate to false
	            mProgressDialog.setIndeterminate(false);
	            mProgressDialog.setMax(100);
	            mProgressDialog.setProgress(progress[0]);
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            mProgressDialog.dismiss();
	            if (result != null)
	                Toast.makeText(context,"Unable to download", Toast.LENGTH_LONG).show();
	            else {
	                // Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
	                 ((VM_Box_Activity)activity).listenToVMClick(curView);
	            }
	        }        
	    }    
	    
	    private class DeleteVMTask extends AsyncTask<String, Integer, String> {

	        private Context context;	        
	        private String curFilename;
	        private Activity activity;

	        public DeleteVMTask(Context context, String FN, Activity a) {
	            this.context = context;
	            this.curFilename = FN;
	            this.activity = a;
	        }
	        
	        @Override
	        protected String doInBackground(String... sUrl) {
	           HttpURLConnection connection = null; 
	           try {
	              URL url = new URL(sUrl[0]);
	           //   Log.i(TAG, "doInBackground "+url);
	              connection = (HttpURLConnection) url.openConnection();
	              connection.connect();
	           //   Log.i(TAG, "doInBackground "+connection.getResponseCode());
                  if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                      return "Server returned HTTP " + connection.getResponseCode() 
                          + " " + connection.getResponseMessage();
	           }
               catch (IOException ignored) { }
               if (connection != null) connection.disconnect();               
  	          return null;	
	        }
	        
	        @Override
	        protected void onPostExecute(String result) {
               //
	        } 	        
	    }
	    
	    private class MyTask extends AsyncTask<String, Void, String> 
	    {
	        @Override
	        protected String doInBackground(String... params) 
	        {
	          Map<String, String> param = new HashMap<String, String>();
	                	param.put("RegID", URLEncoder.encode(getRegID(getBaseContext())));
	                	param.put("num", URLEncoder.encode(getSharedPhone(getBaseContext())));
	                	param.put("BoxNumber", URLEncoder.encode(params[0]));
	                	param.put("BoxPIN", URLEncoder.encode(params[1]));
	                	param.put("nick", URLEncoder.encode(getNickName(getBaseContext()))); 
	                	param.put("Type", URLEncoder.encode("1"));
	                	param.put("pin", URLEncoder.encode(getSharedPIN(getBaseContext())));            	
	                	try {
	    					return ServerUtilities.post("https://api.voiceshot.com/gcm/AppDialer.asp",param);
	    				} catch (IOException e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    					return "";
	    				}          
	        }

	        @Override
	        protected void onPostExecute(String result) 
	        {
	          processValue(result);
	        }
	    }  	    
	      
	    private void processValue(String result){
	         if (result.equals("")||result.equals("notfound")) {
	        	 Toast.makeText(getApplicationContext(), "Unable to call.  Go to settings and tap Configure App.", Toast.LENGTH_LONG).show();	 
	         } else {
	    	   startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(PHONE_DIAL_ACTION_ONE+getDNIS(getBaseContext())+PHONE_DIAL_ACTION_TWO+result+PHONE_DIAL_ACTION_FOUR)));
	         }	    	
	    }
}




