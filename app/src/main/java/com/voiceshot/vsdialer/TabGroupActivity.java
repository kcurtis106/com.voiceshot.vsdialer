package com.voiceshot.vsdialer;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.view.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;


public class TabGroupActivity extends ActivityGroup {

    private ArrayList<String> mIdList;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);          
        if (mIdList == null) mIdList = new ArrayList<String>();
        startChildActivity("Voicemail boxes", new Intent(this,vm_new_box_activity.class));
    }
  
 
  @Override
  public void finishFromChild(Activity child) {
      LocalActivityManager manager = getLocalActivityManager();
      int index = mIdList.size()-1;
      
      if (index < 1) {
          finish();
          return;
      }
          
      manager.destroyActivity(mIdList.get(index), true);
      mIdList.remove(index); index--;
      String lastId = mIdList.get(index);
      Intent lastIntent = manager.getActivity(lastId).getIntent();
      Window newWindow = manager.startActivity(lastId, lastIntent);
      setContentView(newWindow.getDecorView());
  }
  
  @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    return getLocalActivityManager().getCurrentActivity().onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
	    return getLocalActivityManager().getCurrentActivity().onMenuItemSelected(featureId, item);
	}  
  

  public void startChildActivity(String Id, Intent intent) {     
      Window window = getLocalActivityManager().startActivity(Id,intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
      if (window != null) {
          mIdList.add(Id);
          View v = window.getDecorView();          
          setContentView(v); 
      }    
  }
  
  public void setDial(String inNum) {
	  ((DialNumbers)getParent()).setVsDialNumber(inNum);
	  ((DialNumbers)getParent()).tabHost.setCurrentTab(0);	  
  }
  
  /**
   * The primary purpose is to prevent systems before android.os.Build.VERSION_CODES.ECLAIR
   * from calling their default KeyEvent.KEYCODE_BACK during onKeyDown.
   */
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
      if (keyCode == KeyEvent.KEYCODE_BACK) {
          //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
          return true;
      }
      return super.onKeyDown(keyCode, event);
  }

  /**
   * Overrides the default implementation for KeyEvent.KEYCODE_BACK 
   * so that all systems call onBackPressed().
   */
  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
      if (keyCode == KeyEvent.KEYCODE_BACK) {
          onBackPressed();
          return true;
      }
      return super.onKeyUp(keyCode, event);
  }
  
  /**
   * If a Child Activity handles KeyEvent.KEYCODE_BACK.
   * Simply override and add this method.
   */
  @Override
  public void  onBackPressed  () {
      int length = mIdList.size();
      if ( length > 1) {
          Activity current = getLocalActivityManager().getActivity(mIdList.get(length-1));
          current.finish();
      }  
  }
}

