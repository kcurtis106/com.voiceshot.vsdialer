/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.voiceshot.vsdialer;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.voiceshot.vsdialer";
  public static final String BUILD_TYPE = "customDebugType";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 22;
  public static final String VERSION_NAME = "1.20db";
}
